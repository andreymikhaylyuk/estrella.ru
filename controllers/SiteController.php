<?php

namespace app\controllers;

use app\models\Callback;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Pages;
use yii\data\Pagination;
use app\models\User;



class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        //echo Yii::$app->security->generatePasswordHash('garden-stone');die;
        
        $this->addCallback();

        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays policy page.
     *
     * @return string
     */
    public function actionPolicy()
    {
        return $this->render('policy');
    }


    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

     /**
     * Displays ecommerce page.
     *
     * @return string
     */
    public function actionEcommerce()
    {
        return $this->render('ecommerce');
    }

     /**
     * Displays crm page.
     *
     * @return string
     */
    public function actionCrm()
    {
        return $this->render('crm');
    }

        /**
     * Displays Corporate page.
     *
     * @return string
     */
    public function actionCorporate()
    {
        return $this->render('corporate');
    }
         /**
     * Displays mobile page.
     *
     * @return string
     */
    public function actionMobile()
    {
        return $this->render('mobile');
    }




    public function actionServices()
    {
        // return $this->render('services');
        // выполняем запрос
        $this->addCallback();

        $model = Pages::find()->where(['category' => 'Услуги'])->andWhere(['status' => '1'])->all();

        return $this->render('services', [
            'models' => $model,
        ]);



    }

    public function actionProjects()
    {
        $this->addCallback();

        // return $this->render('services');
        // выполняем запрос
        $models = Pages::find()->where(['category' => 'Проекты'])->andWhere(['status' => '1'])->all();

        return $this->render('projects', [
            'models' => $models,
        ]);



    }
    public function actionStages()
    {
        $this->addCallback();

        // return $this->render('services');
        // выполняем запрос
        $query = Pages::find()->where(['category' => 'Этапы работы'])->andWhere(['status' => '1']);
        // делаем копию выборки
        //var_dump($query);
        $countQuery = clone $query;
        // подключаем класс Pagination, выводим по 10 пунктов на страницу
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'defaultPageSize' => 1]);
        // приводим параметры в ссылке к ЧПУ
        $pages->pageSizeParam = false;

        $models = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
        // Передаем данные в представление
        return $this->render('pages', [
            'models' => $models,
            'pages' => $pages,
        ]);



    }
    public function actionAdvantages()
    {
        $this->addCallback();

        // return $this->render('services');
        // выполняем запрос
        $query = Pages::find()->where(['category' => 'Преимущества'])->andWhere(['status' => '1']);
        // делаем копию выборки
        //var_dump($query);
        $countQuery = clone $query;
        // подключаем класс Pagination, выводим по 10 пунктов на страницу
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'defaultPageSize' => 1]);
        // приводим параметры в ссылке к ЧПУ
        $pages->pageSizeParam = false;

        $models = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
        // Передаем данные в представление
        return $this->render('pages', [
            'models' => $models,
            'pages' => $pages,
        ]);



    }

    public function actionContacts()
    {
        $this->addCallback();

        // return $this->render('services');
        // выполняем запрос
        $query = Pages::find()->where(['category' => 'Контакты'])->andWhere(['status' => '1']);
        // делаем копию выборки
        //var_dump($query);
        $countQuery = clone $query;
        // подключаем класс Pagination, выводим по 10 пунктов на страницу
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'defaultPageSize' => 1]);
        // приводим параметры в ссылке к ЧПУ
        $pages->pageSizeParam = false;

        $models = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
        // Передаем данные в представление
        return $this->render('pages', [
            'models' => $models,
            'pages' => $pages,
        ]);



    }

    private function addCallback(){


        $model = new Callback();
        $model->surname = Yii::$app->request->post('surname');
        $model->phone = Yii::$app->request->post('phone');
        $model->status = Callback::STATUS_NEW;
        if($model->save()){

            Yii::$app->session->setFlash('contactFormSubmitted','message',false);
            return $this->refresh();

        } else {
            //var_dump($model->errors);die;
        }


    }

    public function actionProjectsAbout($id)
    {
        $this->addCallback();

        $model = Pages::find()->where(['id' => $id])->andWhere(['status' => '1'])->one();

        return $this->render('projects_about', [
            'model' => $model,
        ]);

    }


}
