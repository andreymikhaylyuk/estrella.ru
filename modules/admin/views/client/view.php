<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Client */

$this->title = $model->surname;
$this->params['breadcrumbs'][] = ['label' => 'Клиенты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="client-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('<i class="fas fa-pen"></i> Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<i class="fas fa-plus"></i> Добавить объект', ['objects/create', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<i class="fas fa-trash-alt"></i> Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить запись?',
                'method' => 'post',
            ],
        ]) ?>


    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'surname',
            'phone',
            'address',
        ],


    ]) ?>

    <h1>Объекты</h1>

    <?php foreach ($objects as $obj): ?>

        <p>
            <?= Html::a('Изменить', \yii\helpers\Url::toRoute(['objects/update', 'id_client' => $obj->id_client, 'id' => $obj->id]), ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Удалить', ['objects/delete', 'id' => $obj->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Вы уверены, что хотите удалить запись?',
                    'method' => 'post',
                ],

            ]) ?>
        </p>

        <?= DetailView::widget([
        'model' => $obj,
        'attributes' => [
            'address',
            'info',
            'service_info',

        ],


    ]) ?>

        <div class="row">
            <div class="col-md-2">
                Фото объекта до
            </div>
            <?php $image = $obj->getImageByName('before'); ?>
            <div class="col-md-10">
                <?= Html::img(Yii::getAlias('@web/upload/photos/').$image->filePath, ['class'=>'img-responsive img-thumbnail', 'style' => 'height: 400px;'])?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                Фото объекта после
            </div>
            <?php $image = $obj->getImageByName('after'); ?>
            <div class="col-md-10">
                <?= Html::img(Yii::getAlias('@web/upload/photos/').$image->filePath, ['class'=>'img-responsive img-thumbnail', 'style' => 'height: 400px;'])?>
            </div>
        </div>

    <?php endforeach; ?>
</div>
