<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\modules\admin\models\Pages;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\PagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $name;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pages-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('<i class="fas fa-plus"></i> Добавить', ['pages/create', 'name' => $name], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'category',
            'title',
            'content:ntext',
//            'image:ntext',
            [
                'header' => 'Фотография',
                'value' => function($data) {
                    $photo = $data->getImage();
                    return '<img src="'.Yii::getAlias('@web/upload/photos/').$photo->filePath.'" class="img-thumbnail">';
                },
                'format' => 'html'
            ],
            'status' => [
                'attribute' => 'status',
                'value' => function($data) {

                    $label = Pages::getStatusValue($data->status);
                    return $label;
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
<style>
    img.img-thumbnail {
        width: 500px;
height: 200px;
object-fit: cover;
    }
</style>