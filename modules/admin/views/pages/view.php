<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Pages */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Страницы', 'url' => ['index', 'name' => $model->category]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pages-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'category',
            'title',
            'content:ntext',

            'status',
        ],
    ]) ?>
    <div class="row">
        <?php foreach ($model->getImages() as $image): ?>
        <div class="col-md-12">
                <?= Html::img(Yii::getAlias('@web/upload/photos/').$image->filePath, ['class'=>'img-responsive', 'style' => 'height: 400px;'])?>
        </div>
        <?php endforeach ?>
    </div>

</div>
