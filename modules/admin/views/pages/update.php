<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Pages */

$this->title = 'Изменить';

$this->params['breadcrumbs'][] = ['label' => 'Страницы', 'url' => ['index-pages']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pages-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'name' => $model->category,

    ]) ?>

</div>
