<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
// use dosamigos\ckeditor\CKEditor;
use \mihaildev\ckeditor\CKEditor;
use \mihaildev\elfinder\ElFinder;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Pages */
/* @var $form yii\widgets\ActiveForm */
if($model->isNewRecord){
    $model->category = $name;
}

?>

<div class="pages-form">
    

    <h2><?= Html::encode($name) ?></h2>

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); 
        
        if ($name == 'Услуги' || $name == 'Проекты') {
            echo $form->field($model, 'category')->hiddenInput()->label(false);
        }
        else {
            echo $form->field($model, 'category')->dropDownList([
                'Этапы работы' => 'Этапы работы',
                'Контакты' => 'Контакты',
                'Преимущества' => 'Преимущества',
            ]);
        }
    ?>


    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?/*= $form->field($model, 'content')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'full'
    ]) */?>
    
    <?= $form->field($model, 'content')->widget(CKEditor::className(), [
                                    'editorOptions' => ElFinder::ckeditorOptions('elfinder', ['height' => '600']),
                            ]) ?>
                            
    <?= $form->field($model, 'image')->fileInput() ?>

    <div class="row">
        <?php foreach ($model->getImages() as $image): ?>

            <?php if (!empty($image->modelName)): ?>
                <div class="col-md-12">
                    <?= Html::img(Yii::getAlias('@web/upload/photos/').$image->filePath, ['class'=>'img-responsive', 'style' => 'height: 400px;'])?>
                </div>
            <?php endif; ?>
        <?php endforeach ?>
    </div>

    <?= $form->field($model, 'status')->dropDownList(\app\modules\admin\models\Pages::getStatusArray()) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
