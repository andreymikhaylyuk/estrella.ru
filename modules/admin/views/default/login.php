<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Вход';
//$this->params['breadcrumbs'][] = $this->title;
?>
<style type="text/css">
    nav#w0.navbar-inverse.navbar-fixed-top.navbar {
        display: none;
    }
</style>
<div class="site-login" style="margin-top: 10%;">
    <div class="col-md-12">
        <div class="col-md-3"></div>
        <div class="col-md-5">
            <div style="padding: 30px;padding-top: 10px;box-shadow: 0px 10px 20px 0px rgba(0, 0, 0, 0.2);border-radius: 10px;">
    <h3 style="margin-bottom: 0;"><?= Html::encode($this->title) ?></h3>

    <p style="font-family: Roboto;font-weight: 300;">Введите логин и пароль</p>

    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-12\">{input}</div>\n<div style=\"margin-top: 10px;margin-left: 15px;\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>

        <?= $form->field($model, 'username')->textInput(['autofocus' => true, 'value' => 'admin'])->label('Логин') ?>

        <?= $form->field($model, 'password')->passwordInput(['value' => 'admin31'])->label('Пароль') ?>

        <?= $form->field($model, 'rememberMe')->checkbox([
            'template' => "<div class=\"col-md-12\">{input} {label}</div>\n<div style=\"margin-top: 10px;margin-left: 15px;\">{error}</div>",
        ])->label('Запомнить меня') ?>

        <div class="form-group">
            <div style="padding-right: 15px;padding-left: 15px;">
                <?= Html::submitButton('Войти', ['class' => 'btn btn-primary', 'name' => 'login-button', 'style' => 'width:100%']) ?>
            </div>
        </div>

    <?php ActiveForm::end(); ?>
</div>
    </div>
</div>
  
</div>
