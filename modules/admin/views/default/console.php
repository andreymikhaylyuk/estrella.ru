<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\CallbackSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Консоль';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="callback-index">

    <h1><?= Html::encode($this->title) ?></h1>


    <div class="panel  panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Количество клиентов: <?= Html::encode($clientCount) ?></h3>
        </div>
    </div>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p></p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            'surname',
            'phone',

//            'status',
            /*[
                'class' => 'yii\grid\CheckboxColumn',
                'header' => 'Ответ на звонок',
                'checkboxOptions' => function ($model, $key, $index, $column) {
                    return ['checked' => $model->answer];
                }
                // you may configure additional properties here
            ],*/


        ],
    ]); ?>
    <?= Html::endForm();?>

</div>