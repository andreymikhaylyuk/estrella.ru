<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Objects */


$this->title = 'Добавить объект';
$this->params['breadcrumbs'][] = ['label' => 'Клиент', 'url' => ['client/view', 'id' => $id_client]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="objects-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'id_client' => $id_client,

    ]) ?>

</div>
