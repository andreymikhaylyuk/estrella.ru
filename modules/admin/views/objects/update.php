<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Objects */

$this->title = 'Изменить объект: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Клиент', 'url' => ['client/view', 'id' => $model->id_client]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="objects-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
