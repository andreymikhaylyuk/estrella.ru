<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Objects */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Добавить объект';
if($model->isNewRecord){
    $model->id_client = $id_client;
}

?>

<div class="objects-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'info')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'service_info')->textarea(['rows' => 6]) ?>


    <?= $form->field($model, 'imageBefore')->fileInput() ?>

    <div class="row">
        <?php $image = $model->getImageByName('before'); ?>
        <?php if (!empty($image->modelName)): ?>
        <div class="col-md-12">
            <?= Html::img(Yii::getAlias('@web/upload/photos/').$image->filePath, ['class'=>'img-responsive', 'style' => 'height: 400px;'])?>
        </div>
        <?php endif; ?>
    </div>

    <?= $form->field($model, 'imageAfter')->fileInput() ?>

    <div class="row">
        <?php $image = $model->getImageByName('after'); ?>
        <?php if (!empty($image->modelName)): ?>
            <div class="col-md-12">
                <?= Html::img(Yii::getAlias('@web/upload/photos/').$image->filePath, ['class'=>'img-responsive', 'style' => 'height: 400px;'])?>
            </div>
        <?php endif; ?>
    </div>


    <?= $form->field($model, 'id_client')->hiddenInput()->label(false) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
