<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \app\models\Callback;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\CallbackSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заявки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="callback-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?=Html::beginForm(['callback/bulk'],'post');?>
        <?=Html::submitButton('Отметить как отвеченные', ['class' => 'btn btn-info',]);?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'surname',
            'phone',
            [
                'attribute' => 'created_at',
                'format' => 'datetime',
            ],
            

            'status' => [
                'attribute' => 'status',
                'value' => function($data) {
                    if ($data->status == Callback::STATUS_NEW) {
                        $color = 'primary';
                    } elseif ($data->status == Callback::STATUS_OK) {
                        $color = 'info';
                    } else {
                        $color = 'info';
                    }
                    $label = '<span class="label label-'.$color.'">'.Callback::getStatusValue($data->status).'</span>';
                    return $label;
                },
                'format' => 'raw',
            ],
            [
                'class' => 'yii\grid\CheckboxColumn',
                'header' => 'Ответ на звонок',
                'checkboxOptions' => function ($model, $key, $index, $column) {
                    return ['checked' => $model->answer];
                }
                // you may configure additional properties here
            ],

            ['class' => 'yii\grid\ActionColumn'],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Клиент',
                'template' => '{link}',
                'buttons' => [
                            'link' => function ($url, $model, $key) {
                            return Html::a('Добавить', \yii\helpers\Url::toRoute(['client/create-from-call', 'surname' => $model->surname, 'phone' => $model->phone]), ['class' => 'btn btn-success']);
                            }
                ]
            ],


        ],
    ]); ?>
    <?= Html::endForm();?>

</div>
