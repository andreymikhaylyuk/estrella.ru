<?php

namespace app\modules\admin\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "pages".
 *
 * @property int $id
 * @property string $category
 * @property string $title
 * @property string $content
 * @property string $image
 * @property int $status
 */
class Pages extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_EDIT = 0;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pages';
    }
    public function behaviors()
    {
        return [
            'image' => [
                'class' => 'rico\yii2images\behaviors\ImageBehave',
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['content'], 'string'],
            [['status'], 'integer'],
            [['category'], 'string', 'max' => 50],
            [['title'], 'string', 'max' => 100],
            [['image'], 'file', 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category' => 'Категория',
            'title' => 'Название',
            'content' => 'Содержание',
            'image' => 'Изображение',
            'status' => 'Статус',
        ];
    }

    /**
     * Загрузка фотографии
     * @return bool
     */
    public function upload()
    {
        if ($this->validate()) {
//            $path = 'upload/photos/' . $this->image->baseName . '.' . $this->image->extension;
            $path = Yii::getAlias('@webroot/upload/photos/') . $this->image->baseName . '.' . $this->image->extension;
            $this->image->saveAs($path);
            $this->attachImage($path, true);
            @unlink($path);
            return true;
        } else {
            return false;
        }
    }

    public static function getStatusArray()
    {
        return [
            self::STATUS_ACTIVE => 'Активная',
            self::STATUS_EDIT => 'Черновик',
        ];
    }

    public static function getStatusValue($status)
    {
        return ArrayHelper::getValue(self::getStatusArray(), $status);
    }
}
