<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "client".
 *
 * @property int $id
 * @property string $surname
 * @property string $phone
 * @property string $address
 *
 * @property Objects[] $objects
 */
class Client extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'client';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['surname', 'phone', 'address'], 'required'],
            [['surname'], 'string', 'max' => 100],
            [['phone'], 'string', 'max' => 20],
            [['address'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'surname' => 'ФИО',
            'phone' => 'Номер телефона',
            'address' => 'Адрес',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObjects()
    {
        return $this->hasMany(Objects::className(), ['id_client' => 'id']);
    }
}
