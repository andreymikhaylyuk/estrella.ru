<?php

namespace app\modules\admin\models;


use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use \app\models\Callback as BaseCallback;

/**
 * This is the model class for table "callback".
 *
 * @property int $id
 * @property string $surname
 * @property string $phone
 * @property int $answer
 * @property string $status
 * @property int $created_at
 */
class Callback extends BaseCallback
{

}
