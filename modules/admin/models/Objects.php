<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "object".
 *
 * @property int $id
 * @property string $address
 * @property string $info
 * @property string $service_info
 * @property string $image_before
 * @property string $image_after
 * @property int $id_client
 *
 * @property Client $client
 */
class Objects extends \yii\db\ActiveRecord
{

    public $imageBefore;
    public $imageAfter;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'imageBefore' => [
                'class' => 'rico\yii2images\behaviors\ImageBehave',
            ],
            'imageAfter' => [
                'class' => 'rico\yii2images\behaviors\ImageBehave',
            ],

        ];
    }
    public static function tableName()
    {
        return 'object';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['info', 'service_info'], 'string'],
            [['id_client'], 'integer'],
            [['address'], 'string', 'max' => 100],
            [['image'], 'file', 'extensions' => 'png, jpg', 'maxFiles' => 5],

            [['id_client'], 'exist', 'skipOnError' => true, 'targetClass' => Client::className(), 'targetAttribute' => ['id_client' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'address' => 'Адрес',
            'info' => 'Информация',
            'service_info' => 'Информация об услуге',
            'image' => 'Изображение',
            'id_client' => 'Id клиента',
            'imageBefore' => 'Изображение до',
            'imageAfter' => 'Изображение после',

        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['id' => 'id_client']);
    }

    public function upload()
    {
        if ($this->validate()) {
            $this->imageBefore->saveAs('web/upload/' . $this->imageBefore->baseName . '.' . $this->imageBefore->extension);
            return true;
        } else {
            return false;
        }
    }
}
