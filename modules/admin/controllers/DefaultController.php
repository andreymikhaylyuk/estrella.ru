<?php

namespace app\modules\admin\controllers;

use app\models\LoginForm;
use app\models\User;

use app\modules\admin\models\Callback;
use app\modules\admin\models\CallbackSearch;
use app\modules\admin\models\Client;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;

/**
 * Default controller for the `admin` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */

    public function behaviors(){
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['login'],
                        'allow' => true,
                        'roles' => ['?']
                    ]

                ],
            ],
        ];
    }
    public function actionIndex()
    {
        $searchModel = new CallbackSearch();
        $query = Callback::find()->where(['answer' => 0])->limit(4);
        //var_dump($query);
        $dataProvider =  new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,

        ]);
        $client = Client::find()->all();
        $clientCount = count($client);
        return $this->render('console', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'clientCount' => $clientCount,
        ]);
        //return $this->render('console');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionConsole() {

        $searchModel = new CallbackSearch();
        $query = Callback::find()->where(['answer' => 0])->limit(4);
        //var_dump($query);
        $dataProvider =  new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,

        ]);
        $client = Client::find()->all();
        $clientCount = count($client);
        return $this->render('console', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'clientCount' => $clientCount,
        ]);
    }

    public function actionAddAdmin() {
        $model = User::find()->where(['username' => 'admin'])->one();
        if (empty($model)) {
            $user = new User();
            $user->username = 'admin';
            $user->email = 'mail@mail.ru';
            $user->setPassword('admin');
            $user->generateAuthKey();
            if ($user->save()) {
                echo 'good';
            }
        }
        var_dump($user);
    }

    
}
