<?php

namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\models\Pages;
use app\modules\admin\models\PagesSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\UploadImage;
use yii\web\UploadedFile;


/**
 * PagesController implements the CRUD actions for Pages model.
 */
class PagesController extends Controller
{
    const CATEGORY_USLUGI = 'Услуги';
    const CATEGORY_ETAPY_RABOTY = 'Этапы работы';
    const CATEGORY_PROEKTY = 'Проекты';
    const CATEGORY_KONTAKTY = 'Контакты';
    const CATEGORY_PREIMUSHESTVA = 'Преимущества';
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['login'],
                        'allow' => true,
                        'roles' => ['?']
                    ]

                ],
            ],
        ];
    }

    /**
     * Lists all Pages models.
     * @return mixed
     */
    public function actionIndex($name)
    {
        $searchModel = new PagesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $name);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'name' => $name,

        ]);
    }

    /**
     * Displays a single Pages model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Добавление
     *
     * @param $name
     * @return string|\yii\web\Response
     */
    public function actionCreate($name)
    {
        $model = new Pages();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->image = UploadedFile::getInstance($model, 'image');

            if ($model->image) {
                $model->upload();
            }

/*            $model->image = UploadedFile::getInstance($model, 'image');

            if ($model->image) {
                $path = Yii::getAlias('@webroot/upload/files/').$model->image->baseName.'.'.$model->image->extension;
                $model->image->saveAs($path);
                $model->attachImage($path);
                //$model->image = $model->image->baseName . '.' . $model->imageBefore->extension;

            }*/
            if ($model->category == self::CATEGORY_ETAPY_RABOTY) {
                return $this->redirect(['pages/index-pages']);
            } elseif  ($model->category == self::CATEGORY_KONTAKTY) {
                return $this->redirect(['pages/index-pages']);
            } elseif  ($model->category == self::CATEGORY_PREIMUSHESTVA) {
                return $this->redirect(['pages/index-pages']);
            } elseif  ($model->category == self::CATEGORY_PROEKTY) {
                return $this->redirect(['pages/index-projects']);
            } elseif  ($model->category == self::CATEGORY_USLUGI) {
                return $this->redirect(['pages/index-services']);
            } else {
                return $this->redirect(['/']);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'name' => $name,
        ]);
    }

    /**
     * Редактирование
     *
     * @param $id
     * @return string|\yii\web\Response
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        //$name = $model->category;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $model->image = UploadedFile::getInstance($model, 'image');
            if ($model->image) {
                $model->upload();
            }

            /*$model->image = UploadedFile::getInstance($model, 'image');
            if ($model->image) {
                $path = Yii::getAlias('@webroot/upload/files/').$model->image->baseName.'.'.$model->image->extension;
                $model->image->saveAs($path);
                $model->attachImage($path);
            }*/

            return $this->redirect(['view', 'id' => $model->id]);

        }
        return $this->render('update', [
            'model' => $model,
            //'name' => $name,
        ]);
    }

    /**
     * Удаление
     *
     * @param $id
     * @return \yii\web\Response
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $name = $model->category;
        $this->findModel($id)->delete();
        //var_dump($name);

        if($name == 'Услуги') {
            return $this->redirect(['index-services', 'name' => $name]);
        }
        elseif ($name == 'Проекты') {
            return $this->redirect(['index-projects', 'name' => $name]);
        }
        else {
            return $this->redirect(['index-pages', 'name' => $name]);
        }
        // return $this->redirect(['index', 'name' => $name]);
    }

    /**
     * Finds the Pages model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pages the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Pages::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionIndexServices()
    {
        $searchModel = new PagesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, 'Услуги');


        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'name' => 'Услуги'
        ]);
    }
    public function actionIndexProjects()
    {
        $searchModel = new PagesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, 'Проекты');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'name' => 'Проекты'
        ]);
    }

    public function actionIndexPages()
    {
        $searchModel = new PagesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, 'Страницы');


        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'name' => 'Страницы'
        ]);
    }
}
