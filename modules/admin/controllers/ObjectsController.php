<?php

namespace app\modules\admin\controllers;

use app\models\UploadImage;
use Yii;
use app\modules\admin\models\Objects;
use app\modules\admin\models\ObjectsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ObjectsController implements the CRUD actions for Objects model.
 */
class ObjectsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['login'],
                        'allow' => true,
                        'roles' => ['?']
                    ]

                ],
            ],

        ];
    }

    /**
     * Lists all Objects models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ObjectsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Objects model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Objects model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        //var_dump($id);
        $model = new Objects();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //var_dump($model->image);
                $model->imageBefore = UploadedFile::getInstance($model, 'imageBefore');
                $model->imageAfter = UploadedFile::getInstance($model, 'imageAfter');
                //var_dump($model->image);
                if ($model->imageBefore) {
                    $pathB = Yii::getAlias('@webroot/upload/files').$model->imageBefore->baseName.'.'.$model->imageBefore->extension;
                    $model->imageBefore->saveAs($pathB);
                    $model->attachImage($pathB, true,'before');
                    //$model->image = $model->image->baseName . '.' . $model->imageBefore->extension;
    
                }
                if ($model->imageAfter) {
                    $path = Yii::getAlias('@webroot/upload/files').$model->imageAfter->baseName.'.'.$model->imageAfter->extension;
                    $model->imageAfter->saveAs($path);
                    $model->attachImage($path, true, 'after');
                    //$model->image = $model->image->baseName . '.' . $model->imageBefore->extension;
    
                }
                return $this->redirect(['client/view', 'id' => $model->id_client]);
       }
       return $this->render('create', [
        'model' => $model,
        'id_client' => $id,
        ]);
    }

    /**
     * Updates an existing Objects model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        /*if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);*/

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //var_dump($model->image);
                $model->imageBefore = UploadedFile::getInstance($model, 'imageBefore');
                $model->imageAfter = UploadedFile::getInstance($model, 'imageAfter');
                //var_dump($model->image);
                if ($model->imageBefore) {
                    $path = Yii::getAlias('@webroot/upload/files').$model->imageBefore->baseName.'.'.$model->imageBefore->extension;
                    $model->imageBefore->saveAs($path);
                    $model->attachImage($path);
                    //$model->image = $model->image->baseName . '.' . $model->imageBefore->extension;
    
                }
                if ($model->imageAfter) {
                    $path = Yii::getAlias('@webroot/upload/files').$model->imageAfter->baseName.'.'.$model->imageAfter->extension;
                    $model->imageAfter->saveAs($path);
                    $model->attachImage($path);
                    //$model->image = $model->image->baseName . '.' . $model->imageBefore->extension;
    
                }
                return $this->redirect(['client/view', 'id' => $model->id_client]);


        }
      
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Objects model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        
        $model = $this->findModel($id);
        $model->removeImages();
        $model->delete();

        return $this->redirect(['client/view', 'id' => $model->id_client]);
    }

    /**
     * Finds the Objects model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Objects the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Objects::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }


}
