<?php

use yii\db\Migration;

/**
 * Class m180627_154029_create_table_pages
 */
class m180627_154029_create_table_pages extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('pages', [
            'id' => $this->primaryKey(),
            'category' => $this->string(50),
            'title' => $this->string(100),
            'content' => $this->text(),
            'image' => $this->text(),
            'status' => $this->tinyInteger(1)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180627_154029_create_table_pages cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180627_154029_create_table_pages cannot be reverted.\n";

        return false;
    }
    */
}
