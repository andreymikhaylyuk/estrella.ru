<?php

use yii\db\Migration;

/**
 * Class m180629_171140_update_callback_table_add_column_cr_at
 */
class m180629_171140_update_callback_table_add_column_cr_at extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('callback', 'created_at', $this->integer()->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180629_171140_update_callback_table_add_column_cr_at cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180629_171140_update_callback_table_add_column_cr_at cannot be reverted.\n";

        return false;
    }
    */
}
