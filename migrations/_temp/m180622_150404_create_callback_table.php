<?php

use yii\db\Migration;

/**
 * Handles the creation of table `callback`.
 */
class m180622_150404_create_callback_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('callback', [
            'id' => $this->primaryKey(),
            'surname' => $this->string(100)->notNull(),
            'phone' => $this->string(20)->notNull(),
            'answer' => $this->tinyInteger(1),

        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('callback');
    }


}
