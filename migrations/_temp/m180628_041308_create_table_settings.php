<?php

use yii\db\Migration;

/**
 * Class m180628_041308_create_table_settings
 */
class m180628_041308_create_table_settings extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('settings', [
            'id' => $this->primaryKey(),
            'name' => $this->string(100),
            'content' => $this->string(100),

        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180628_041308_create_table_settings cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180628_041308_create_table_settings cannot be reverted.\n";

        return false;
    }
    */
}
