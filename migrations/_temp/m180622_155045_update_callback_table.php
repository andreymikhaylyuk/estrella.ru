<?php

use yii\db\Migration;

/**
 * Class m180622_155045_update_callback_table
 */
class m180622_155045_update_callback_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('callback', 'status', $this->string(64));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180622_155045_update_callback_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180622_155045_update_callback_table cannot be reverted.\n";

        return false;
    }
    */
}
