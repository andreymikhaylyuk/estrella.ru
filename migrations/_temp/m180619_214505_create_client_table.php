<?php

use yii\db\Migration;

/**
 * Handles the creation of table `client`.
 */
class m180619_214505_create_client_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('client', [
            'id' => $this->primaryKey(),
            'surname' => $this->string(100)->notNull(),
            'phone' => $this->string(20)->notNull(),
            'address' => $this->string(50)->notNull(),

        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('client');
    }
}
