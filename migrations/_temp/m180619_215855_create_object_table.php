<?php

use yii\db\Migration;

/**
 * Handles the creation of table `object`.
 */
class m180619_215855_create_object_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('object', [
            'id' => $this->primaryKey(),
            'address' => $this->string(100),
            'info' => $this->text(),
            'service_info' => $this->text(),
            'image' => $this->string(400),
            'id_client' => $this->integer(),

        ]);

        $this->addForeignKey(
            'id_client',
            'object',
            'id_client',
            'client',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('object');

        $this->dropForeignKey(
            'id_client',
            'client'
        );
    }
}
