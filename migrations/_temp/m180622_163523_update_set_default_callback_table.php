<?php

use yii\db\Migration;

/**
 * Class m180622_163523_update_set_default_callback_table
 */
class m180622_163523_update_set_default_callback_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('callback', 'answer', $this->tinyInteger(1)->notNull()->defaultValue(0)->unsigned());

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180622_163523_update_set_default_callback_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180622_163523_update_set_default_callback_table cannot be reverted.\n";

        return false;
    }
    */
}
