<?php

use yii\db\Migration;

class m180702_181701_init_db extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        // 0. Пользователи
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),
            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        // 1. Клиенты
        $this->createTable('client', [
            'id' => $this->primaryKey(),
            'surname' => $this->string(100)->notNull(),
            'phone' => $this->string(20)->notNull(),
            'address' => $this->string(50)->notNull(),

        ], $tableOptions);

        // 2. Объекты
        $this->createTable('object', [
            'id' => $this->primaryKey(),
            'address' => $this->string(100),
            'info' => $this->text(),
            'service_info' => $this->text(),
            'image' => $this->string(400),
            'id_client' => $this->integer(),

        ], $tableOptions);

        $this->addForeignKey(
            'id_client',
            'object',
            'id_client',
            'client',
            'id',
            'CASCADE'
        );

        // 3. Обратные звонки
        $this->createTable('callback', [
            'id' => $this->primaryKey(),
            'status' => $this->smallInteger(1)->notNull(),
            'answer' => $this->tinyInteger(1)->notNull()->defaultValue(0)->unsigned(),
            'surname' => $this->string(100)->notNull(),
            'phone' => $this->string(20)->notNull(),
            'created_at' => $this->integer()->notNull(),

        ], $tableOptions);

        // 4. Страницы
        $this->createTable('pages', [
            'id' => $this->primaryKey(),
            'category' => $this->string(50),
            'title' => $this->string(100),
            'content' => $this->text(),
            'image' => $this->text(),
            'status' => $this->tinyInteger(1)
        ], $tableOptions);

        // 5. Настройки
        $this->createTable('settings', [
            'id' => $this->primaryKey(),
            'name' => $this->string(100),
            'content' => $this->string(100),

        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // 0. Пользователи
        $this->dropTable('user');

        // 1. Клиенты
        $this->dropTable('client');

        // 2. Объекты
        $this->dropTable('object');

        $this->dropForeignKey(
            'id_client',
            'client'
        );

        // 3. Обратные звонки
        $this->dropTable('callback');

        // 4. Страницы
        $this->dropTable('pages');

        // 5. Настройки
        $this->dropTable('settings');
    }
}
