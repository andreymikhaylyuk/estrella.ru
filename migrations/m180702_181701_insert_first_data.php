<?php

use yii\db\Migration;

class m180702_181701_insert_first_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        /**
         * Первичное заполнение таблиц данными
         */

        // 0. Пользователи
        $this->insert('user', [
            'username' => 'admin',
            'auth_key' => '',
            'password_hash' => Yii::$app->security->generatePasswordHash('admin31'),
            'password_reset_token' => '',
            'email' => 'admin@mail.ru',
            'status' => \app\models\User::STATUS_ACTIVE,
            'created_at' => time(),
            'updated_at' => time(),
        ]);

        // 5. Настройки
        $this->insert('settings', ['name' => 'Номер телефона 1', 'content' => '+7 (905) 123-4567']);
        $this->insert('settings', ['name' => 'Номер телефона 2', 'content' => '+7 (905) 123-4568']);
        $this->insert('settings', ['name' => 'email', 'content' => 'mail@mail.ru']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
