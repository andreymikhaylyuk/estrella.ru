<?php

namespace app\models;


use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "callback".
 *
 * @property int $id
 * @property string $surname
 * @property string $phone
 * @property int $answer
 * @property string $status
 * @property int $created_at
 */
class Callback extends \yii\db\ActiveRecord
{

    const STATUS_NEW = 1;
    const STATUS_OK = 2;

    /**
     * {@inheritdoc}
     */

    public function behaviors(){
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
        // если вместо метки времени UNIX используется datetime:
        'value' => function() { return date('U');},
        ]   ,
        ];
       }
    public static function tableName()
    {
        return 'callback';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['surname', 'phone'], 'required'],
            [['answer', 'created_at', 'status'], 'integer'],
            [['surname'], 'string', 'max' => 100],
            [['phone'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'surname' => 'Фамилия',
            'phone' => 'Телефон',
            'answer' => 'Ответ',
            'status' => 'Статус',
            'created_at' => 'Время создания',
        ];
    }

    public static function getStatusArray()
    {
        return [
            self::STATUS_NEW => 'Новая',
            self::STATUS_OK => 'Обработан',
        ];
    }

    public static function getStatusValue($status)
    {
        return ArrayHelper::getValue(self::getStatusArray(), $status);
    }
}

