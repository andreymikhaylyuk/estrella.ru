<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
// библиотеки
        'libs/swiper/dist/css/swiper.min.css',
        'libs/@fancyapps/fancybox/dist/jquery.fancybox.min.css',
        'libs/@fortawesome/fontawesome-free/css/all.min.css',
        'css/quick-website.css',
        'css/owl.carousel.min.css',
        'css/owl.theme.default.min.css',



    ];
    public $js = [
        'libs/jquery/dist/jquery.min.js',
        'libs/bootstrap/dist/js/bootstrap.bundle.min.js',
        'libs/in-view/dist/in-view.min.js',
        'libs/sticky-kit/dist/sticky-kit.min.js',
        'libs/svg-injector/dist/svg-injector.min.js',
        'libs/feather-icons/dist/feather.min.js',
        'libs/imagesloaded/imagesloaded.pkgd.min.js',
        'libs/jquery.scrollbar/jquery.scrollbar.min.js',
        'libs/jquery-scroll-lock/dist/jquery-scrollLock.min.js',
        'libs/swiper/dist/js/swiper.min.js',
        'libs/@fancyapps/fancybox/dist/jquery.fancybox.min.js',
        'js/quick-website.js',
        'https://code.jquery.com/jquery-3.2.1.slim.min.js',
        'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js',
        'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js',
        'js/owl.carousel.min.js',


    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}


  
     

  
   
