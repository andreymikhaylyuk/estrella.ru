<div class="main-container" style="margin-top: 85px">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
<?php

// подключаем виджет постраничной разбивки
use yii\widgets\LinkPager;

// проходим цикл по данным модели
foreach ($models as $model) {
    // выводим название организации (пример)
    // echo $model->title;
    echo $model->content;

} 
//var_dump($pages);

// отображаем постраничную разбивку
echo LinkPager::widget([
    'pagination' => $pages,
]); ?>

</div>