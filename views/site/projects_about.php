<?php


use yii\helpers\Html;

/* var_dump($model);
die; */
?>

<div class="main-container" style="margin-top: 50px;">
    <div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
    <!--<h2> <?= Html::encode($model->title) ?> </h2>-->
        <?php 
            $image = $model->getImage();
        ?>
        <div class="services-images">
        <?= Html::img(Yii::getAlias('@web/upload/photos/').$image->filePath) ?>
        </div>
        <?php 
                echo $model->content;
        ?>
        <?= Html::button("<span class='glyphicon glyphicon-plus' aria-hidden='true'>Вернуться</span>",
                    ['class'=>'button',
                        'onclick'=> "window.location.href = '" . \Yii::$app->urlManager->createUrl(['/site/projects','id'=>$model->id]) . "';",
                    ]
                ) ?> 
<!--         <button type="button" class="button" data-toggle="modal" data-target="#exampleModalCenter">Подробнее</button> -->
</div>
<div class="col-md-2"></div>
</div>
</div>