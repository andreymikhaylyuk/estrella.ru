<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="slice py-8 bg-dark bg-cover bg-size--cover" style="background-image:url(/office.jpg")>
         <span class="mask bg-gradient-dark opacity-7"></span>
         <div data-offset-top=#navbar-main style="padding-top: 0px !important">
            <div class="container d-flex align-items-center text-center text-lg-left py-5">
               <div class="col px-0">
                  <div class="row row-grid align-items-center">
                     <div class="col-lg-8 text-center text-lg-left">
                        <h1 class="text-white mb-4">Интернет-агентство Estrella</h1>
                        <p class="lead text-white opacity-8">Создаем мощные сайты для вашего бизнеса с высокой конверсией, которые легко продвинуть в Яндекс и Google. Полностью ведем проект или помогаем команде на стороне клиента.</p>

                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- <div class="shape-container shape-line shape-position-bottom"><img alt="Image placeholder" src="/img/svg/separators/line.svg" class=svg-inject></div> -->
      </section>

       <section class="slice slice-lg">
         <div class="container text-left">
            <div class=row>
               <div class=col-md-12>
                  <span class="badge badge-primary badge-pill font-weight-normal">about</span>
                  <h5 class="h3 lh-180 mt-4 mb-4">О нас</h5>
               </div>
            </div>
         </div>
         <div class=container>
          
            <div class="row mt-4">
               <div class="col-lg-8 pr-lg-5">
                  <p class="text-black lh-180 mb-3">Компания DIGITAL оказывает услуги в четырех основных направлениях: разработка и дизайн сайтов, оптимизация и продвижение сайтов, организация рекламы в интернете.</p>
<p class="text-black lh-180 mb-3">
За время работы мы реализовали более 18 000 интернет-проектов от простых сайтов-визиток до сложных IT-порталов и сервисов. Ежемесячно запускается более 2 000 рекламных кампаний. 520 клиентов стали лидерами своей тематики вместе с нами.</p>
<p class="text-black lh-180 mb-3">
Внимательное отношение к клиентам, оперативность и слаженность в работе, нестандартные и креативные решения, современная технологическая база и собственные разработки, профессиональные специалисты, которые в совершенстве владеют интернет-технологиями — главные составляющие, формирующие лицо нашей компании.</p>
<p class="text-black lh-180 mb-3">
Благодаря своим преимуществам мы получаем самое ценное — доверие наших клиентов.</p>
               </div>
               <div class=col-lg-4>
                  <p class="h1 text-black mb-0 font-size-55">2012</p>
                  <p class="text-black mt-0">год основания</p>
                 
               </div>
            </div>
         </div>
      </section>

      <section class="slice slice-lg">
         <div class="container text-left">
            <div class=row>
               <div class=col-md-12>
                  <span class="badge badge-primary badge-pill font-weight-normal">about</span>
                  <h5 class="h3 lh-180 mt-4 mb-4">Что мы делаем</h5>
               </div>
            </div>
         </div>
         <div class=container>
          
            <div class="row mt-4">
               <div class="col-lg-4 pr-lg-5">
                  <p class="text-black lh-180 mb-3">Компания DIGITAL оказывает услуги в четырех основных направлениях: разработка и дизайн сайтов, оптимизация и продвижение сайтов, организация рекламы в интернете.</p>
               </div>
               <div class="col-lg-4 pr-lg-5">
                  <p class="text-black lh-180 mb-3">Компания DIGITAL оказывает услуги в четырех основных направлениях: разработка и дизайн сайтов, оптимизация и продвижение сайтов, организация рекламы в интернете.</p>
               </div>
               <div class="col-lg-4 pr-lg-5">
                  <p class="text-black lh-180 mb-3">Компания DIGITAL оказывает услуги в четырех основных направлениях: разработка и дизайн сайтов, оптимизация и продвижение сайтов, организация рекламы в интернете.</p>
               </div>
            </div>
            <div class="row mt-4">
               <div class="col-lg-4 pr-lg-5">
                  <p class="text-black lh-180 mb-3">Компания DIGITAL оказывает услуги в четырех основных направлениях: разработка и дизайн сайтов, оптимизация и продвижение сайтов, организация рекламы в интернете.</p>
               </div>
               <div class="col-lg-4 pr-lg-5">
                  <p class="text-black lh-180 mb-3">Компания DIGITAL оказывает услуги в четырех основных направлениях: разработка и дизайн сайтов, оптимизация и продвижение сайтов, организация рекламы в интернете.</p>
               </div>
               <div class="col-lg-4 pr-lg-5">
                  <p class="text-black lh-180 mb-3">Компания DIGITAL оказывает услуги в четырех основных направлениях: разработка и дизайн сайтов, оптимизация и продвижение сайтов, организация рекламы в интернете.</p>
               </div>
            </div>
         </div>
      </section>
      
<section class="section-half-rounded mt-6 mb-6">
   <div class="jumbotron section-inner right-0 rounded-left bg-primary overflow-hidden col-lg-7"><img src="/img/bg_ecommerce_try.jpg" alt="Image" class="img-as-bg blend-mode--multiply"></div>
   <div class="container text-center text-lg-left py-5">
      <div class="row">
         <div class="col-lg-7">
            <div class="card shadow-lg rounded-lg mb-0">
               <div class="px-5 py-6">
                  <h2 class="h3 font-weight-bold mb-5">Творческий подход в дизайне<span class="text-primary typed" id="type-example-1" data-type-this="love., patience., dedication.">|</span></h2>
                  <div class="row">
                  	<div class="col-lg-6">
                  		<p class="lead text-sm text-black"><span class="text-primary typed font-weight-bold text-md">-</span> Создаем современный, удобный и эффективный дизайн под задачи вашего бизнеса</p>
                  	</div>
                  	<div class="col-lg-6">
                  		<p class="lead text-sm text-black"><span class="text-primary typed font-weight-bold text-md">-</span> Решительно беремся за самые трудоемкие и креативные задачи и блестяще с ними справляемся.</p>
                  	</div>
                  	<div class="col-lg-6">
                  		<p class="lead text-sm text-black"><span class="text-primary typed font-weight-bold text-md">-</span> Тщательно прорабатываем каждую деталь. Когда мы беремся, например, за разработку логотипа, мы добиваемся лучшего результата из всех возможных</p>
                  	</div>
                  	<div class="col-lg-6">
                  		<p class="lead text-sm text-black"><span class="text-primary typed font-weight-bold text-md">-</span> Мы не бросаем слова на ветер, поэтому мы всегда укладываемся в сроки и действуем согласно плану.</p>
                  	</div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>


       <section class="slice slice-lg">
         <div class="container text-left">
            <div class=row>
               <div class=col-md-12>
                  <span class="badge badge-primary badge-pill font-weight-normal">about</span>
                  <h5 class="h3 lh-180 mt-4 mb-4">Качественное обслуживание вас и ваших проектов</h5>
               </div>
            </div>
         </div>
         <div class=container>
          
            <div class="row mt-4">
               <div class="col-lg-4 pr-lg-5">
                  <p class="text-black lh-180 mb-3">Компания DIGITAL оказывает услуги в четырех основных направлениях: разработка и дизайн сайтов, оптимизация и продвижение сайтов, организация рекламы в интернете.</p>
               </div>
               <div class="col-lg-4 pr-lg-5">
                  <p class="text-black lh-180 mb-3">Компания DIGITAL оказывает услуги в четырех основных направлениях: разработка и дизайн сайтов, оптимизация и продвижение сайтов, организация рекламы в интернете.</p>
               </div>
               <div class="col-lg-4 pr-lg-5">
                  <p class="text-black lh-180 mb-3">Компания DIGITAL оказывает услуги в четырех основных направлениях: разработка и дизайн сайтов, оптимизация и продвижение сайтов, организация рекламы в интернете.</p>
               </div>
            </div>
            <div class="row mt-4">
               <div class="col-lg-4 pr-lg-5">
                  <p class="text-black lh-180 mb-3">Компания DIGITAL оказывает услуги в четырех основных направлениях: разработка и дизайн сайтов, оптимизация и продвижение сайтов, организация рекламы в интернете.</p>
               </div>
               <div class="col-lg-4 pr-lg-5">
                  <p class="text-black lh-180 mb-3">Компания DIGITAL оказывает услуги в четырех основных направлениях: разработка и дизайн сайтов, оптимизация и продвижение сайтов, организация рекламы в интернете.</p>
               </div>
               <div class="col-lg-4 pr-lg-5">
                  <p class="text-black lh-180 mb-3">Компания DIGITAL оказывает услуги в четырех основных направлениях: разработка и дизайн сайтов, оптимизация и продвижение сайтов, организация рекламы в интернете.</p>
               </div>
            </div>
         </div>
      </section>


<section class="section-half-rounded mt-6 mb-6">
   <div class="jumbotron section-inner left-0 rounded-right bg-primary overflow-hidden col-lg-7"><img src="/img/bg_ecommerce_try.jpg" alt="Image" class="img-as-bg blend-mode--multiply"></div>
   <div class="container text-center text-lg-left py-5">
      <div class="row">
         <div class="col-lg-7 ml-auto">
            <div class="card shadow-lg rounded-lg mb-0">
               <div class="px-5 py-6">
                  <h2 class="h3 font-weight-bold mb-5">Современные
технологии в
разработке<span class="text-primary typed" id="type-example-1" data-type-this="love., patience., dedication.">|</span></h2>
                  <div class="row">
                  	<div class="col-lg-6">
                  		<p class="lead text-sm text-black"><span class="text-primary typed font-weight-bold text-md">-</span> Создаем проекты, максимально отвечающие потребностям вашего бизнеса</p>
                  	</div>
                  	<div class="col-lg-6">
                  		<p class="lead text-sm text-black"><span class="text-primary typed font-weight-bold text-md">-</span> Непрерывно повышаем свою квалификацию: изучаем, обучаем, внедряем</p>
                  	</div>
                  	<div class="col-lg-6">
                  		<p class="lead text-sm text-black"><span class="text-primary typed font-weight-bold text-md">-</span> Используем современные системы автоматизации процессов - DashBoard, GIT</p>
                  	</div>
                  	<div class="col-lg-6">
                  		<p class="lead text-sm text-black"><span class="text-primary typed font-weight-bold text-md">-</span> Работаем над проектами по методике Scrum</p>
                  	</div>
                  	<div class="col-lg-6">
                  		<p class="lead text-sm text-black"><span class="text-primary typed font-weight-bold text-md">-</span> Используем новейшие технологии и современные технологические решения: html5, php7, less, yii2.</p>
                  	</div>
                  	<div class="col-lg-6">
                  		<p class="lead text-sm text-black"><span class="text-primary typed font-weight-bold text-md">-</span> Тщательно тестируем все проекты в Jenkins</p>
                  	</div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>


      <section class="slice slice-lg pt-10 bg-primary">
         <div class="shape-container shape-line shape-position-top shape-orientation-inverse"><img alt="Image placeholder" src="/img/svg/separators/line.svg" class=svg-inject></div>
         <div class=container>
            <div class="row row-grid">
               <div class=col-lg-8>
                  <span class="badge badge-warning badge-pill">Made with Bootstrap</span>
                  <h2 class="my-4 text-white">Websites, Dashboards and Web Apps</h2>
                  <p class="lead text-white lh-190">Build one product from the ground up using our dedicated tools. A beautiful ecosystem built to sustain productivity and encourage developers to create modern and professional high-end products.</p>
               </div>
            </div>
            <div class="row mt-6">
               <div class=col-lg-6>
                  <div class=row>
                     <div class=col-sm-4>
                        <div class="card shadow-lg rounded-lg border-0 mb-sm-0">
                           <div class="p-4 text-center text-sm-left">
                              <h3 class=mb-0><span class=counter data-from=0 data-to=10 data-speed=3000 data-refresh-interval=200></span> <span class=counter-extra>k</span></h3>
                              <p class="text-muted mb-0">Customers</p>
                           </div>
                        </div>
                     </div>
                     <div class=col-sm-4>
                        <div class="card shadow-lg rounded-lg border-0 mb-sm-0">
                           <div class="p-4 text-center text-sm-left">
                              <h3 class=mb-0><span class=counter data-from=0 data-to=53 data-speed=3000 data-refresh-interval=200></span> <span class=counter-extra>k</span></h3>
                              <p class="text-muted mb-0">Downloads</p>
                           </div>
                        </div>
                     </div>
                     <div class=col-sm-4>
                        <div class="card shadow-lg rounded-lg border-0 mb-sm-0">
                           <div class="p-4 text-center text-sm-left">
                              <h3 class=mb-0><span class=counter data-from=0 data-to=98 data-speed=3000 data-refresh-interval=200></span> <span class=counter-extra>%</span></h3>
                              <p class="text-muted mb-0">Happy users</p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="slice slice-lg pb-5">
         <div class=container>
            <div class="row mb-6 justify-content-center text-center">
               <div class="col-lg-8 col-md-10">
                  <h2 class=mt-4>The amazing team</h2>
               </div>
            </div>
            <div class=row>
               <div class="col-lg-3 col-sm-6 mb-5">
                  <div data-animate-hover=2>
                     <div class=animate-this><a href="about.html#"><img alt="Image placeholder" class="img-fluid rounded shadow" src="/img/theme/light/person-1.jpg"></a></div>
                     <div class=mt-3>
                        <h5 class="h6 mb-0">Heather Wright</h5>
                        <p class="text-muted text-sm mb-0">Founded &amp; Chairman</p>
                     </div>
                  </div>
               </div>
               <div class="col-lg-3 col-sm-6 mb-5">
                  <div data-animate-hover=2>
                     <div class=animate-this><a href="about.html#"><img alt="Image placeholder" class="img-fluid rounded shadow" src="/img/theme/light/person-2.jpg"></a></div>
                     <div class=mt-3>
                        <h5 class="h6 mb-0">Monroe Parker</h5>
                        <p class="text-muted text-sm mb-0">Back End Developer</p>
                     </div>
                  </div>
               </div>
               <div class="col-lg-3 col-sm-6 mb-5">
                  <div data-animate-hover=2>
                     <div class=animate-this><a href="about.html#"><img alt="Image placeholder" class="img-fluid rounded shadow" src="/img/theme/light/person-3.jpg"></a></div>
                     <div class=mt-3>
                        <h5 class="h6 mb-0">John Sullivan</h5>
                        <p class="text-muted text-sm mb-0">Front End Developer</p>
                     </div>
                  </div>
               </div>
               <div class="col-lg-3 col-sm-6 mb-5">
                  <div data-animate-hover=2>
                     <div class=animate-this><a href="about.html#"><img alt="Image placeholder" class="img-fluid rounded shadow" src="/img/theme/light/person-4.jpg"></a></div>
                     <div class=mt-3>
                        <h5 class="h6 mb-0">James Lewis</h5>
                        <p class="text-muted text-sm mb-0">Vice Chairman</p>
                     </div>
                  </div>
               </div>
            </div>
            <div class=row>
               <div class="col-lg-3 col-sm-6 mb-5 mb-sm-0">
                  <div data-animate-hover=2>
                     <div class=animate-this><a href="about.html#"><img alt="Image placeholder" class="img-fluid rounded shadow" src="/img/theme/light/person-5.jpg"></a></div>
                     <div class=mt-3>
                        <h5 class="h6 mb-0">Danielle Levin</h5>
                        <p class="text-muted text-sm mb-0">Sales Manager</p>
                     </div>
                  </div>
               </div>
               <div class="col-lg-3 col-sm-6 mb-5 mb-sm-0">
                  <div data-animate-hover=2>
                     <div class=animate-this><a href="about.html#"><img alt="Image placeholder" class="img-fluid rounded shadow" src="/img/theme/light/person-6.jpg"></a></div>
                     <div class=mt-3>
                        <h5 class="h6 mb-0">Martin Gray</h5>
                        <p class="text-muted text-sm mb-0">UI/UX Designer</p>
                     </div>
                  </div>
               </div>
               <div class="col-lg-3 col-sm-6 mb-5 mb-sm-0">
                  <div data-animate-hover=2>
                     <div class=animate-this><a href="about.html#"><img alt="Image placeholder" class="img-fluid rounded shadow" src="/img/theme/light/person-7.jpg"></a></div>
                     <div class=mt-3>
                        <h5 class="h6 mb-0">George Squier</h5>
                        <p class="text-muted text-sm mb-0">Marketing Executive</p>
                     </div>
                  </div>
               </div>
               <div class="col-lg-3 col-sm-6 mb-0">
                  <div data-animate-hover=2>
                     <div class=animate-this><a href="about.html#"><img alt="Image placeholder" class="img-fluid rounded shadow" src="/img/theme/light/person-8.jpg"></a></div>
                     <div class=mt-3>
                        <h5 class="h6 mb-0">Jesse Stevens</h5>
                        <p class="text-muted text-sm mb-0">Ads Manager</p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section>
         <div class="container mb-5 py-5 border-top border-bottom">
            <div class="row align-items-center">
               <div class="col-12 col-md">
                  <h3 class="h5 mb-1">We are hiring!</h3>
                  <p class="font-size-lg text-muted mb-4 mb-md-0">If you want to be a part of this awesome journey we’d love to hear from you!</p>
               </div>
               <div class="col-12 col-md-auto"><a href="about.html#" class="btn btn-primary">Send your CV </a><a href="about.html#" class="btn btn-neutral btn-icon"><span class=btn-inner--text>See jobs</span> <span class=btn-inner--icon><i data-feather=arrow-right></i></span></a></div>
            </div>
         </div>
      </section>

      <style type="text/css">
	nav.navbar.navbar-main.navbar-expand-lg.navbar-dark.bg-black {
		background-color: #fff !important;
	}
	a.nav-link.dropdown-toggle.font-weight-normal {
		color: #222 !important;

	}
	a.nav-link.font-weight-normal {
		color: #222 !important;
		font-size: 14px;
	}
</style>