<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use app\widgets\Setting;
use app\widgets\setcall\SetCall;


?>
<section class="py-7 py-lg-0 spotlight bg-gradient-video" data-spotlight=fullscreen>
                  <video autoplay loop muted>
                      <source src="Pexels.mp4" type="video/mp4">
                      </video>
         <div class=spotlight-holder data-offset-top=#navbar-main>
            <div class="container d-flex align-items-center">
               <div class="col px-0">
                  <div class="row row-grid align-items-center">
                     <div class="col-lg-10 text-center text-lg-left">
                        <h1 class="display-4 text-white my-4">Создание и продвижение сайтов, услуги интернет-маркетинга</h1>
                        <p class="lead text-white text-sm">Build a beautiful, modern website with flexible Bootstrap components built from scratch.</p>
                        <div class=mt-5><a href="#" class="btn btn-white btn-lg btn-icon" data-toggle="modal" data-target="#exampleModalCenter"><span class="btn-inner--text font-weight-normal">Начать проект</span></a></div>
                       
                     </div>
                  
                  </div>
               </div>
            </div>
         </div>

 
      </section>


      <section style="background-color: #000;">
         <div class="row">
            <div class="col-lg-3" style="padding: 0px;">
               <a href="#" class="hre_top_service">
                  <div class="home_service_top" style="background: url(developers.jpg);">
                     <p class="p_text_service_home">
                     Веб разработка
                  </p>
                  </div>
               </a>
            </div>
            <div class="col-lg-3" style="padding: 0px;">
               <a href="#" class="hre_top_service">
                  <div class="home_service_top" style="background: url(sales.jpg);">
                     <p class="p_text_service_home">
                     Продвижение
                  </p>
                  </div>
               </a>
            </div>
            <div class="col-lg-3" style="padding: 0px;">
               <a href="#" class="hre_top_service">
                  <div class="home_service_top" style="background: url(designs.jpg);">
                     <p class="p_text_service_home">
                     Дизайн
                  </p>
                  </div>
               </a>
            </div>
            <div class="col-lg-3" style="padding: 0px;">
               <a href="#" class="hre_top_service">
                  <div class="home_service_top" style="background: url(mobile.jpg);">
                     <p class="p_text_service_home">
                     Мобильные приложения
                  </p>
                  </div>
               </a>
            </div>
            
         <div>   
      </section>

      <section class="py-10 py-lg-10" style="background: url(IMG_80131.jpg);background-size: cover;background-position: center;">
         <div class="container">
            <div class="row">
               <div class="col-lg-7">
                  <h3 class="text-black">Лучшие решения из мира DIGITAL</h3>
                  <p class="text-black font-size-18">Создаем сайты с высокой конверсией, которые легко вывести в ТОП. Полностью ведем проект или помогаем команде на стороне клиента. Вы можете обратиться в интернет-агентство DIGITAL и если точно понимаете, каким должен быть ваш ресурс, и тем более, если понятия не имеете, что на нем должно быть. Если у вас уже есть сайт — мы его оптимизируем, превратим в полноценный и эффективный инструмент бизнеса.</p>
               </div>
               <div class="col-lg-5 text-center pt-5">
                  <img src="siteawards.png" style="height: 120px;">
                  <img src="cssawards.png" style="height: 120px;">
                  <img src="webguruawwards.png" style="height: 120px;">
               </div>
            </div>
         </div>
      </section>
      


      

      <section class="py-10">
      
         <div class="container">
               <div class="row">
                  <div class="col-lg-4"><h2>Почему наши сайты хорошо продают</h2></div>
                  <div class="col-lg-8">
                     <div class="row">
                        <div class="col-lg-6">
                           <div class="block_home_new">
                              <p class="font-weight-bold-new">Гибкая структура</p>
                              <p class="text-black">
                                 Наши сайты – это динамичная, живая структура, которая отражает все изменения вашего бизнеса, растет вместе с ним. Именно поэтому мы предусмотрели возможность быстрой модификации ресурса: вы сможете изменить конфигурацию, добавить новые разделы, модули, каталоги.
                              </p>
                           </div>
                           <div class="block_home_new">
                              <p class="font-weight-bold-new">Простота и эффективность</p>
                              <p class="text-black">
                                 Наши сайты максимально лаконичны и просты. Мы используем только эффективные приемы для продажи вашего товара или услуги и не добавляем ничего лишнего.
                              </p>
                           </div>
                        </div>
                        <div class="col-lg-6">
                           <div class="block_home_new">
                              <p class="font-weight-bold-new">Дорабатываем каждую мелочь</p>
                              <p class="text-black">
                                 Создание сайтов — это дело нашей жизни, а не просто заработок, мы не относимся к своей работе равнодушно и буквально «горим» каждым проектом, дорабатывая каждую мелочь.
                              </p>
                           </div>
                           <div class="block_home_new">
                              <p class="font-weight-bold-new">Адаптивность</p>
                              <p class="text-black">
                                 Сайты максимально адаптивны и соответствуют требованиям поисковой оптимизации, они без труда покоряют топы поисковой выдачи Яндекс и Google.
                              </p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
         </div>

      </section>
<section class="py-10" style="background-color: #000;">
     
         <div class="container">
                  <div class="col-lg-12 text-center mb-7">
                     <h2 class="text-white">Нам доверяют</h2>
                  </div>
                  <div class="owl-carousel">
                      <div> <img src="logo/arendator.png"> </div>
                      <div> <img src="logo/bissel.png"> </div>
                      <div> <img src="logo/bork.png"> </div>
                      <div> <img src="logo/calipso.png"> </div>
                      <div> <img src="logo/craftcoffee.png"> </div>
                      <div> <img src="logo/cresh копия.png"> </div>
                      <div> <img src="logo/deadline_kvest_logo.png"> </div>
                      <div> <img src="logo/gm.png"> </div>
                      <div> <img src="logo/karelinfinance_eng.png"> </div>
                      <div> <img src="logo/koncert.png"> </div>
                      <div> <img src="logo/Koritca-logo.png"> </div>
                      <div> <img src="logo/logo-aks.png"> </div>
                      <div> <img src="logo/Micoffe.png"> </div>
                      <div> <img src="logo/myata_logo.png"> </div>
                      <div> <img src="logo/novaia-logo.png"> </div>
                      <div> <img src="logo/okonteh.png"> </div>
                      <div> <img src="logo/parshina.png"> </div>
                      <div> <img src="logo/proplivi_logo.png"> </div>
                      <div> <img src="logo/sberbank.png"> </div>
                      <div> <img src="logo/todes.png"> </div>
                      <div> <img src="logo/volkova.png"> </div>
                      <div> <img src="logo/x-jet.png"> </div>
                      <div> <img src="logo/yubicon.png"> </div>
                      <div> <img src="logo/zehnder.png"> </div>


                  </div>
               </div>
            </section>
     <section class="slice slice-lg delimiter-bottom">
         <div class=container>
            <div class="col-lg-12 text-center mb-7">
                     <h2 class="text-black">Отзывы клиентов</h2>
                  </div>
            <div class="row justify-content-center">
               <div class=col-lg-9>
                  
                  <div class=swiper-js-container>
                     <div class=swiper-container data-swiper-items=1 data-swiper-space-between=0>
                        <div class=swiper-wrapper>
                           <div class=swiper-slide>
                              <div class=text-center>
                                 <p class="text-black lh-180 px-4 font-weight-300">"Amazing looking theme and instantly turns your application into a professional looking one. Really shows that professionals have built this theme up. Very happy with the way the theme looks and the example pages are very good."</p>
                                 <div class="text-center mt-4"><span class="h6 font-weight-light">Zehnder</span> <span class="font-weight-light">- Стариков Вадим</span></div>
                              </div>
                           </div>
                           <div class=swiper-slide>
                              <div class=text-center>
                                 <p class="text-black lh-180 px-4 font-weight-300">"Amazing looking theme and instantly turns your application into looking theme and instantly turns your application into looking theme and instantly turns your application into a professional looking one. Really shows that professionals have built this theme up. Very happy with the way the theme looks and the example pages are very good.] "</p>
                                 <div class="text-center mt-4"><span class="h6 font-weight-light">Todes</span> <span class="font-weight-light">- Лебедев Тимур</span></div>
                              </div>
                           </div>
                           <div class=swiper-slide>
                              <div class=text-center>
                                 <p class="text-black lh-180 px-4 font-weight-300">"Amazing looking theme and instantly turns your application into a professional looking one. Really shows that professionals have built this theme up. Very happy with the way the theme looks and the example pages are very good."</p>
                                 <div class="text-center mt-4"><span class="h6 font-weight-light">BORK</span> <span class="font-weight-light">- Степаненко Даниил</span></div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="swiper-button swiper-button-next btn-dark zindex-101"></div>
                     <div class="swiper-button swiper-button-prev btn-dark zindex-101"></div>
                  </div>
               </div>
            </div>
         </div>
      </section>



      <section class="slice pt-5 pb-7 bg-section-secondary">
             <div class="col-lg-12 text-center mb-7">
                     <h2 class="text-black">Новости</h2>
                  </div>
         <div class=container>
            <div class=row>
              <?= \app\widgets\services\ServicesWidget::widget() ?>
               <!-- <div class="col-xl-4 col-md-6">
                  <div class="card hover-translate-y-n3 hover-shadow-lg overflow-hidden">
                     <div class="position-relative overflow-hidden"><a href="#" class=d-block><img alt="Image placeholder" src="img/theme/light/blog-1-800x600.jpg" class=card-img-top></a></div>
                     <div class="card-body py-4">
                        <small class="d-block text-sm mb-2">25 April, 2019</small> <a href="#" class="h5 stretched-link lh-150">Choose the best solution for your business</a>
                        <p class="mt-3 mb-0 lh-170">No matter what he does, every person on earth plays a central role in the history.</p>
                     </div>
                  </div>
               </div>
               <div class="col-xl-4 col-md-6">
                  <div class="card hover-translate-y-n3 hover-shadow-lg overflow-hidden">
                     <div class="position-relative overflow-hidden"><a href="#" class=d-block><img alt="Image placeholder" src="img/theme/light/blog-1-800x600.jpg" class=card-img-top></a></div>
                     <div class="card-body py-4">
                        <small class="d-block text-sm mb-2">25 April, 2019</small> <a href="#" class="h5 stretched-link lh-150">Choose the best solution for your business</a>
                        <p class="mt-3 mb-0 lh-170">No matter what he does, every person on earth plays a central role in the history.</p>
                     </div>
                  </div>
               </div>
               <div class="col-xl-4 col-md-6">
                  <div class="card hover-translate-y-n3 hover-shadow-lg overflow-hidden">
                     <div class="position-relative overflow-hidden"><a href="#" class=d-block><img alt="Image placeholder" src="img/theme/light/blog-1-800x600.jpg" class=card-img-top></a></div>
                     <div class="card-body py-4">
                        <small class="d-block text-sm mb-2">25 April, 2019</small> <a href="#" class="h5 stretched-link lh-150">Choose the best solution for your business</a>
                        <p class="mt-3 mb-0 lh-170">No matter what he does, every person on earth plays a central role in the history.</p>
                     </div>
                  </div>
               </div>
            </div> -->
            </div>
         </div>
      </section>
<?php if (Yii::$app->session->has('contactFormSubmitted')) { ?>

    <?php
    $this->registerJs(
        "$('#myModalSendOk').modal('show');",
        yii\web\View::POS_READY
    );
    ?>

<?php } ?>
<?php /* 

<section class="section-services">

    <?= \app\widgets\services\ServicesWidget::widget() ?>

</section>

*/ ?>



<?php /* 
<section class="section-services">

    <?= \app\widgets\galery\GaleryWidget::widget() ?>

</section> */ ?>


<?php /*
<section class="section-question">
    <div class="main-container">
        <div class="wrapper">
            <h3>Остались вопросы?</h3>
            <p>Закажите бесплатную консультацию</p>
            <?= SetCall::widget() ?>

        </div>
    </div>
</section>

<section class="section-contacts">
    <div class="main-container">
        <h2>Контакты</h2>
        <span class="phone"><?= Setting::widget(['attr'=>'Номер телефона 1']) ?></span>
        <span class="phone"><?= Setting::widget(['attr'=>'Номер телефона 2']) ?></span>
        <span class="mail"><?= Setting::widget(['attr'=>'email']) ?></span>

    </div>
</section> */ ?>


