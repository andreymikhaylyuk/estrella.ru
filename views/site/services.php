<?php


use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<section class="slice slice-lg py-7">
         <img src="/news.jpg" alt=Image class="img-as-bg">
         <span class="mask bg-gradient-dark opacity-7"></span>
         <div class="container d-flex align-items-center" data-offset-top=#navbar-main>
            <div class="col py-5">
               <div class="row align-items-center justify-content-center">
                  <div class="col-md-7 col-lg-7 text-center">
                     <h1 class="display-4 text-white mb-2">Новости/блог</h1>
                     <span class="text-white text-sm">Пишем о технологиях, новинках из мира IT</span>
                  </div>
               </div>
            </div>
         </div>
      </section>

      <section class="slice pt-5 pb-7 bg-section-secondary">
         <div class="container masonry-container">
            <div class="row mansory">
                 <?php foreach ($models as $model): 
                $image = $model->getImage();
                //die;
                        ?>
               <div class="masonry-item col-xl-4 col-md-6">
                  <div class="card hover-translate-y-n3 hover-shadow-lg overflow-hidden">
                     <div class="position-relative overflow-hidden"><a href="blog-grid.html#" class=d-block>
                        <?= Html::img(Yii::getAlias('@web/upload/photos/').$image->filePath, ['class' => 'card-img-top', 'style' => 'height: 300px;oveflow: hidden;object-fit: cover;']) ?></a></div>
                     <div class="card-body py-4">
                        <a href="blog-grid.html#" class="h5 stretched-link lh-110"><?= Html::encode($model->title) ?></a>
                        <p class="mt-3 mb-0 lh-130" style="height: 60px;overflow: hidden;"><small><?= Html::encode($model->content) ?></small></p>
                     </div>
                    
                  </div>
               </div>
        <?php endforeach; ?>

               


            </div>
            
         </div>
      </section>



<style type="text/css">
    nav.navbar.navbar-main.navbar-expand-lg.navbar-dark.bg-black {
        background-color: #fff !important;
    }
    a.nav-link.dropdown-toggle.font-weight-normal {
        color: #222 !important;

    }
    a.nav-link.font-weight-normal {
        color: #222 !important;
        font-size: 14px;
    }
</style>s