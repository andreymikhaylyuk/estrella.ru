<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Contact';
$this->params['breadcrumbs'][] = $this->title;
?>

<section class="slice py-6 pt-lg-7 pb-lg-8 bg-gradient-primary">
     <img src="/map_contacts.jpg" alt=Image class="img-as-bg">

         <div class="container d-flex align-items-center text-center text-lg-left">
            <div class="col px-0">
               <div class="row row-grid align-items-center">
                  <div class=col-lg-6>
                     <h1 class="h1 text-white text-center text-lg-left my-4">Контакты</h1>
                     <p class="lead text-white text-center text-lg-left opacity-8">Build a beautiful, modern website with flexible Bootstrap components built from scratch.</p>
                     <div class="mt-5 text-center text-lg-left"><a href="#sct-form-contact" data-scroll-to class="btn btn-white btn-lg btn-icon"><span class=btn-inner--text>Напсиать письмо</span></a></div>
                  </div>
               </div>
            </div>
         </div>
      </section>
  <section class="slice slice-lg bg-section-secondary">
         <div class=container>
            <div class=row>
               <div class=col-lg-4>
                  <div class="card text-center hover-translate-y-n10 hover-shadow-lg">
                     <div class="px-5 pb-5">
                        <div class=py-3>
                           
                        </div>
                        <h5 class="">Clients</h5>
                        <p class="mt-2 mb-0">Отдел работы с клиентами<br>пн-пт 10:00-18:00</p>
                        <div class=mt-4>
                            <a href="contact.html#" class=link-underline-primary>
                            hello@estrella.agency</a></div>
                     </div>
                  </div>
               </div>
               <div class=col-lg-4>
                  <div class="card text-center hover-translate-y-n10 hover-shadow-lg">
                     <div class="px-5 pb-5">
                        <div class=py-3>
                        </div>
                        <h5 class="">Support</h5>
                        <p class="mt-2 mb-0">Техническая поддержка действующих клиентов<br>пн-вс 24/7</p>
                        <div class=mt-4><a href="contact.html#" class=link-underline-primary>support@estrella.agency</a></div>
                     </div>
                  </div>
               </div>
               <div class=col-lg-4>
                  <div class="card text-center hover-translate-y-n10 hover-shadow-lg">
                     <div class="px-5 pb-5">
                        <div class=py-3>
                        </div>
                        <h5 class="">Finance</h5>
                        <p class="mt-2 mb-0">Финансовый отдел <br>пн-пт 10:00-16:00</p>
                        <div class=mt-4><a href="contact.html#" class=link-underline-primary>finance@estrella.agency</a></div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="slice slice-lg">
         <div class=container>
            <div class="row row-grid justify-content-between align-items-center">
               <div class=col-lg-5>
                  <h3>308015, Россия, г.Белгород<br>Народный бульвар 70, офис 417</h3>
                  
                  <p class="lead my-4">E: <a href="contact.html#">info@estrella.agency</a><br>T: 8 4722 36 50 21</p>
                  
                  <p><small>Хотите поделиться с нами какой-либо обратной связью, сообщить о технической проблеме или просто задать нам вопрос? Не стесняйтесь обращаться к нам, и мы свяжемся с вами в ближайшее время.</small></p>

               </div>

               <div class=col-lg-6>
<a name="sct-form-contact"></a>
                   <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

        <div class="alert alert-success">
            Thank you for contacting us. We will respond to you as soon as possible.
        </div>

        <p>
            Note that if you turn on the Yii debugger, you should be able
            to view the mail message on the mail panel of the debugger.
            <?php if (Yii::$app->mailer->useFileTransport): ?>
                Because the application is in development mode, the email is not sent but saved as
                a file under <code><?= Yii::getAlias(Yii::$app->mailer->fileTransportPath) ?></code>.
                Please configure the <code>useFileTransport</code> property of the <code>mail</code>
                application component to be false to enable email sending.
            <?php endif; ?>
        </p>

    <?php else: ?>

       
        

                <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>

                    <?= $form->field($model, 'name')->textInput()->label('Как вас зовут?') ?>

                    <?= $form->field($model, 'email') ?>

                    <?= $form->field($model, 'body')->textarea(['rows' => 6])->label('Текст') ?>

                   

                    <div class="form-group">
                        <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                    </div>

                <?php ActiveForm::end(); ?>

            

    <?php endif; ?>
               </div>
            </div>
         </div>
      </section>





<style type="text/css">
    nav.navbar.navbar-main.navbar-expand-lg.navbar-dark.bg-black {
        background-color: #fff !important;
    }
    a.nav-link.dropdown-toggle.font-weight-normal {
        color: #222 !important;

    }
    a.nav-link.font-weight-normal {
        color: #222 !important;
        font-size: 14px;
    }
</style>
