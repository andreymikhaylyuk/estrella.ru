<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Политика конфедициальности';
$this->params['breadcrumbs'][] = $this->title;
?>




<section>
         <div class="container d-flex align-items-center text-center text-lg-left py-5 py-lg-6 border-bottom">
            <div class="col px-0">
               <div class="row justify-content-center">
                  <div class="col-lg-8 text-center">
                     <h2 class="h1 mb-2">Политика конфедициальности</h2>
                     <h6 class="font-weight-light text-muted" style="font-family: 'Cera-Pro-Reg' !important">Обновлено: 26 ноября 2019</h6>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class=slice>
         <div class=container>
            <div class="row">
               <div class=col-lg-2>
                 
               </div>
               <div class=col-lg-8>
                  <div class=mb-5>
                     <h5 id=item-1>Introduction</h5>
                     <p>These Website Standard Terms and Conditions written on this webpage shall manage your use of our website, Webiste Name accessible at Website.com.</p>
                     <p>These Terms will be applied fully and affect to your use of this Website. By using this Website, you agreed to accept all terms and conditions written in here. You must not use this Website if you disagree with any of these Website Standard Terms and Conditions.</p>
                     <p>Minors or people below 18 years old are not allowed to use this Website.</p>
                  </div>
                  <div class=mb-5>
                     <h5 id=item-2>Intellectual Property Rights</h5>
                     <p>Other than the content you own, under these Terms, Company Name and/or its licensors own all the intellectual property rights and materials contained in this Website.</p>
                     <p>You are granted limited license only for purposes of viewing the material contained on this Website.</p>
                  </div>
                  <div class=mb-5>
                     <h5 id=item-3>Restrictions</h5>
                     <p>You are specifically restricted from all of the following:</p>
                     <ul>
                        <li>publishing any Website material in any other media;</li>
                        <li>selling, sublicensing and/or otherwise commercializing any Website material;</li>
                        <li>publicly performing and/or showing any Website material;</li>
                        <li>using this Website in any way that is or may be damaging to this Website;</li>
                        <li>using this Website in any way that impacts user access to this Website;</li>
                        <li>using this Website contrary to applicable laws and regulations, or in any way may cause harm to the Website, or to any person or business entity;</li>
                        <li>engaging in any data mining, data harvesting, data extracting or any other similar activity in relation to this Website;</li>
                        <li>using this Website to engage in any advertising or marketing.</li>
                        <li>Certain areas of this Website are restricted from being access by you and Company Name may further restrict access by you to any areas of this Website, at any time, in absolute discretion. Any user ID and password you may have for this Website are confidential and you must maintain confidentiality as well.</li>
                     </ul>
                  </div>
                  <div class=mb-5>
                     <h5 id=item-4>Your Content</h5>
                     <p>In these Website Standard Terms and Conditions, “Your Content” shall mean any audio, video text, images or other material you choose to display on this Website. By displaying Your Content, you grant Company Name a non-exclusive, worldwide irrevocable, sub licensable license to use, reproduce, adapt, publish, translate and distribute it in any and all media.</p>
                     <p>Your Content must be your own and must not be invading any third-party's rights. Company Name reserves the right to remove any of Your Content from this Website at any time without notice.</p>
                  </div>
                  <div class=mb-5>
                     <h5 id=item-5>No warranties</h5>
                     <p>This Website is provided “as is,” with all faults, and Company Name express no representations or warranties, of any kind related to this Website or the materials contained on this Website. Also, nothing contained on this Website shall be interpreted as advising you.</p>
                  </div>
                  <div class=mb-5>
                     <h5 id=item-6>Limitation of liability</h5>
                     <p>In no event shall Company Name, nor any of its officers, directors and employees, shall be held liable for anything arising out of or in any way connected with your use of this Website whether such liability is under contract. Company Name, including its officers, directors and employees shall not be held liable for any indirect, consequential or special liability arising out of or in any way related to your use of this Website.</p>
                  </div>
                  <div class=mb-5>
                     <h5 id=item-7>Indemnification</h5>
                     <p>You hereby indemnify to the fullest extent Company Name from and against any and/or all liabilities, costs, demands, causes of action, damages and expenses arising in any way related to your breach of any of the provisions of these Terms.</p>
                  </div>
                  <div class=mb-5>
                     <h5 id=item-8>Severability</h5>
                     <p>If any provision of these Terms is found to be invalid under any applicable law, such provisions shall be deleted without affecting the remaining provisions herein.</p>
                  </div>
                  <div class=mb-5>
                     <h5 id=item-9>Assignment</h5>
                     <p>The Company Name is allowed to assign, transfer, and subcontract its rights and/or obligations under these Terms without any notification. However, you are not allowed to assign, transfer, or subcontract any of your rights and/or obligations under these Terms.</p>
                  </div>
                  <div class=mb-5>
                     <h5 id=item-10>Entire Agreement</h5>
                     <p>These Terms constitute the entire agreement between Company Name and you in relation to your use of this Website, and supersede all prior agreements and understandings.</p>
                  </div>
                  <div class=mb-5>
                     <h5 id=item-11>Governing Law & Jurisdiction</h5>
                     <p>These Terms will be governed by and interpreted in accordance with the laws of the State of Country, and you submit to the non-exclusive jurisdiction of the state and federal courts located in Country for the resolution of any disputes.</p>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section>




















<style type="text/css">
	nav.navbar.navbar-main.navbar-expand-lg.navbar-dark.bg-black {
		background-color: #fff !important;
	}
	a.nav-link.dropdown-toggle.font-weight-normal {
		color: #222 !important;

	}
	a.nav-link.font-weight-normal {
		color: #222 !important;
		font-size: 14px;
	}
</style>