<?php


use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
      <section class="slice slice-lg py-7">
         <img src="/projects1.jpg" alt=Image class="img-as-bg">
         <span class="mask bg-gradient-dark opacity-7"></span>
         <div class="container d-flex align-items-center" data-offset-top=#navbar-main>
            <div class="col py-5">
               <div class="row align-items-center justify-content-center">
                  <div class="col-md-7 col-lg-7 text-center">
                     <h1 class="display-4 text-white mb-2">Проекты</h1>
                     <span class="text-white text-sm">projects</span>
                  </div>
               </div>
            </div>
         </div>
      </section>
    
    <section class=border-bottom>
         <div class="container py-3">
            <div class="row align-items-center">
               <div class="col-lg-12 mb-4 mb-lg-0">
                  <ul class=nav>
                     <li class=nav-item><a class="nav-link text- active" href="blog-masonry.html#">Все проекты</a></li>
                     <li class=nav-item><a class="nav-link" href="blog-masonry.html#">eCommerce</a></li>
                     <li class=nav-item><a class=nav-link href="blog-masonry.html#">Landing Page</a></li>
                     <li class=nav-item><a class=nav-link href="blog-masonry.html#">Корпоративные сайты</a></li>
                     <li class=nav-item><a class=nav-link href="blog-masonry.html#">CRM системы</a></li>
                     <li class=nav-item><a class=nav-link href="blog-masonry.html#">Мобильные приложения</a></li>
                     <li class=nav-item><a class=nav-link href="blog-masonry.html#">Дизайн</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </section>

      <section class="slice pt-5 pb-7 bg-section-secondary">
         <div class="container masonry-container">
            <div class="row masonry">
               

               <div class="masonry-item col-xl-4 col-md-6">
                  <div class="card hover-translate-y-n3 hover-shadow-lg overflow-hidden">
                     <div class="position-relative overflow-hidden"><a href="blog-masonry.html#" class=d-block><img alt="Image placeholder" src="/todes-pr.jpg" class=card-img-top></a></div>
                     
                     <div class="card-body position-relative delimiter-bottom">
                        <div class="d-flex align-items-center">
                           <div>
                            <a href="blog-masonry.html#" class="avatar avatar-sm rounded-circle d-inline-block">
                                <img alt="Image placeholder" src="/fav_todes.jpg">
                            </a>
                        </div>
                           <div class="pl-3 col-lg-11"><a href="blog-masonry.html#" class="h6 stretched-link lh-150">Todes - школа танцев</a></div>
                        </div>
                     </div>

                     <div class="pb-0 card-body py-3" style="padding-bottom: 0 !important">
                        <small class="d-block text-sm mb-2">#ecommerce</small> 
                     </div>
                  </div>
               </div>

                <div class="masonry-item col-xl-4 col-md-6">
                  <div class="card hover-translate-y-n3 hover-shadow-lg overflow-hidden">
                     <div class="position-relative overflow-hidden"><a href="blog-masonry.html#" class=d-block><img alt="Image placeholder" src="/bissell.jpg" class=card-img-top></a></div>
                     
                     <div class="card-body position-relative delimiter-bottom">
                        <div class="d-flex align-items-center">
                           <div>
                            <a href="blog-masonry.html#" class="avatar avatar-sm rounded-circle d-inline-block">
                                <img alt="Image placeholder" src="/fav_bissell.jpg">
                            </a>
                        </div>
                           <div class="pl-3 col-lg-11"><a href="blog-masonry.html#" class="h6 stretched-link lh-150">Bissell</a></div>
                        </div>
                     </div>

                     <div class="pb-0 card-body py-3" style="padding-bottom: 0 !important">
                        <small class="d-block text-sm mb-2">#ecommerce</small> 
                     </div>
                  </div>
               </div>

               <div class="masonry-item col-xl-4 col-md-6">
                  <div class="card hover-translate-y-n3 hover-shadow-lg overflow-hidden">
                     <div class="position-relative overflow-hidden"><a href="blog-masonry.html#" class=d-block><img alt="Image placeholder" src="/calipso-pr.jpg" class=card-img-top></a></div>
                     
                     <div class="card-body position-relative delimiter-bottom">
                        <div class="d-flex align-items-center">
                           <div>
                            <a href="blog-masonry.html#" class="avatar avatar-sm rounded-circle d-inline-block">
                                <img alt="Image placeholder" src="/fav_calipso.jpg">
                            </a>
                        </div>
                           <div class="pl-3 col-lg-11"><a href="blog-masonry.html#" class="h6 stretched-link lh-150">Калипсо кофе</a></div>
                        </div>
                     </div>

                     <div class="pb-0 card-body py-3" style="padding-bottom: 0 !important">
                        <small class="d-block text-sm mb-2">#ecommerce</small> 
                     </div>
                  </div>
               </div>



            </div>
            
         </div>
      </section>
      <?php /*
<section class="section-services">
    <div class="main-container">
        <h2> <?= Html::encode($models[0]->category) ?> </h2>
        <div class="row">

            <?php foreach ($models as $model):
                $image = $model->getImage();
            ?>


                <div class="col-lg-4 col-md-6 item">
                    <!--<h3><?//= Html::encode($model->title) ?> </h3>-->
                    <div class="services-images">
                        <?= Html::img(Yii::getAlias('@web/upload/photos/').$image->filePath) ?>
                    </div>
                    <div style="text-align: center">
                    <?= Html::button("<span class='glyphicon glyphicon-plus' aria-hidden='true'>Подробнее</span>",
                    ['class'=>'button',
                        'onclick'=> "window.location.href = '" . \Yii::$app->urlManager->createUrl(['/site/projects-about','id'=>$model->id]) . "';",
                    ]
                    ) ?> 
                    </div>

                </div>

            <?php endforeach; ?>
        </div>


    </div>
</section> */ ?>

<style type="text/css">
    nav.navbar.navbar-main.navbar-expand-lg.navbar-dark.bg-black {
        background-color: #fff !important;
    }
    a.nav-link.dropdown-toggle.font-weight-normal {
        color: #222 !important;

    }
    a.nav-link.font-weight-normal {
        color: #222 !important;
        font-size: 14px;
    }
</style>

