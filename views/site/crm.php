<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'CRM системы';
$this->params['breadcrumbs'][] = $this->title;
?>

<section class="py-7 py-lg-7" style="background: url(/crm.jpg);background-size: cover;height: 65vh;background-repeat: no-repeat;object-fit: cover;width: 100%;background-position: bottom;">
	<div class="container d-flex align-items-center">
               <div class="col px-0">
                  <div class="row row-grid align-items-center">
                     <div class="col-lg-10 text-center text-lg-left">
                     	<p class="text-white">CRM</p>
                        <h1 class="display-4 text-white my-4">CRM системы</h1>
                        <p class="lead text-white text-md">Разработаем для вас интернет-магазин под ключ, с мощным функционалом, удобным интерфейсом, привлекательным дизайном и мобильной версией.</p>
                        <div class="mt-5"><a href="#" class="btn btn-white btn-lg btn-icon"><span class="btn-inner--text font-weight-normal">Начать проект</span></a></div>
                     </div>
                  </div>
               </div>
            </div>
</section>
      <section class="section-half-rounded pt-5 pb-0">
         <div class="jumbotron section-inner left-0 rounded-bottom-right bg-light overflow-hidden col-lg-11"></div>
         <div class="container text-center text-lg-left">
            <div class=row>
               <div class="col-lg-10 col-md-10">
                  <p class="lh-180 text-black mt-4 mb-6">Мы занимаемся разработкой проектов любой сложности и оказываем комплексные услуги в области интернет-маркетинга. Вы платите 1 раз за продающий интернет-магазин, который генерирует посетителей 24/7. Доказательством этому служит высокая конверсия на наших работах, в среднем 10-12%.</h5>
               </div>
            </div>
         </div>
      </section>

<style type="text/css">
	nav.navbar.navbar-main.navbar-expand-lg.navbar-dark.bg-black {
		background-color: #fff !important;
	}
	a.nav-link.dropdown-toggle.font-weight-normal {
		color: #222 !important;

	}
	a.nav-link.font-weight-normal {
		color: #222 !important;
		font-size: 14px;
	}
</style>
<section class="py-5 py-lg-5 mt-5">
	<div class="container text-center">
            <div class=row>
               <div class=col-md-12>
                  <span class="badge badge-primary badge-pill font-weight-normal">cooperation</span>
                  <h5 class="h3 lh-180 mt-4 mb-6">5 причин сотрудничать с нами</h5>
               </div>
            </div>
         </div>
	<!-- <div class="container d-flex align-items-center">
               <div class="col px-0">
                  <div class="row row-grid align-items-center">
                     <div class="col-lg-12 text-center text-lg-left">
                        <h1 class="display-6 text-center font-weight-normal text-black my-4" style="font-family: 'Cera-Pro-Reg' !important">5 причин сотрудничать с нами</h1>
                     </div>
                  </div>
               </div>
            </div> -->

    <div class="container d-flex align-items-center">
               <div class="col px-0">
                  <div class="row row-grid mt-5">
                     <div class="col-lg-4 text-center text-lg-left">
                        <p class="text-black">SEO-специалисты, участвующие в разработке интернет-магазина, собирают семантическое ядро и проводят внутреннюю оптимизацию. Готовый сайт легко выходит в топ по основным ключевым запросам.</p>
                     </div>
                     <div class="col-lg-4 text-lg-left">
                        <p class="text-black">Мы создаем простую и понятную систему управления сайтом. В дальнейшем функционал интернет-магазина можно будет легко скорректировать или доработать.</p>
                     </div>
                     <div class="col-lg-4 text-lg-left">
                        <p class="text-black">Мы создаем адаптивные сайты с удобным современным дизайном. Глубокая проработка всех элементов юзабилити, гарантирует вам до 12%конверсии. В результате, вы экономите на рекламе, а количество звонков и заявок растет.</p>
                     </div>
                     <div class="col-lg-4 text-center text-lg-left">
                        <p class="text-black">Сдавая заказчику готовый проект, мы ответим на все вопросы и предоставим подробную инструкцию по управлению интернет-магазином.</p>
                     </div>
                     <div class="col-lg-4 text-center text-lg-left">
                        <p class="text-black">В результате нашей работы вы получите сайт, созданный в 100% соответствии с требованиями законодательства РФ (то есть, вам не придется бояться штрафов).</p>
                     </div>
                  </div>
               </div>
            </div>
           
</section>

<section class="section-half-rounded mt-6 mb-6">
   <div class="jumbotron section-inner left-0 rounded-right bg-primary overflow-hidden col-lg-9"><img src="/img/bg_ecommerce_try.jpg" alt="Image" class="img-as-bg blend-mode--multiply"></div>
   <div class="container text-center text-lg-left py-5">
      <div class="row">
         <div class="col-lg-7 ml-auto">
            <div class="card shadow-lg rounded-lg mb-0">
               <div class="px-5 py-6">
                  <h2 class="h3 font-weight-bold mb-3">Что вы получите<span class="text-primary typed" id="type-example-1" data-type-this="love., patience., dedication.">|</span></h2>
                  <p class="lead text-sm text-black">Мы предлагаем разработку интернет-магазина, который будет полностью соответствовать требованиям поисковых систем. При этом произведем настройку сервисов аналитики, подключим модули ведения электронной торговли (оплата в онлайн-режиме, поиск похожих товаров, фильтры и т.д.).</p>
                  <div class="mt-5">
                     <a href="studio.html#" class="btn btn-warning btn-icon shadow-sm hover-translate-y-n3">
                        <span class="btn-inner--text">Заказать</span> 
                        <span class="btn-inner--icon">
                           <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right">
                              <line x1="5" y1="12" x2="19" y2="12"></line>
                              <polyline points="12 5 19 12 12 19"></polyline>
                           </svg>
                        </span>
                     </a>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>

<section class="py-5 py-lg-5">
	<div class="container text-center">
            <div class=row>
               <div class=col-md-12>
                  <span class="badge badge-primary badge-pill font-weight-normal">stages</span>
                  <h5 class="h3 lh-180 mt-4 mb-3">Этапы</h5>
               </div>
            </div>
         </div>
	<!-- <div class="container d-flex align-items-center">

               <div class="col px-0">
                  <div class="row row-grid align-items-center">
                     <div class="col-lg-12 text-center text-lg-left">
                        <h1 class="display-6 text-center font-weight-normal text-black my-4" style="font-family: 'Cera-Pro-Reg' !important">Этапы</h1>
                     </div>
                  </div>
               </div>
            </div> -->
    <div class="container d-flex align-items-center">

               <div class="col px-0 mt-5">
                  <div class="row justify-content-between mb-5">
                  	<div class="col-lg-3 text-lg-right">
                  		<h4 class="mb-4 text-primary">1. Заполнение <br>брифа</h4>
                  	</div>
                  	<div class="col-lg-8 text-black">
                  		<p>Прежде чем приступить к разработке интернет-магазина, мы просим клиента максимально подробно ответить на вопросы из брифа. Нам важно знать, какие из действующих проектов вы считаете наиболее удачными. Так мы сможем лучше понимать ваши желания и эстетические представления. От этой информации мы будем отталкиваться на этапе разработки макета. Кроме того, обязательно подробно опишите сферу вашей деятельности: какие услуги/товары/направления являются для вас приоритетными. Мы сделаем на них акцент.</p>
                  		<p>Отдельно сообщите о ваших цветовых предпочтениях и пожеланиях относительно центрального баннера на сайте(что должно быть размещено, какие образы, объекты, люди или услуги/товары должны быть указаны). На этом этапе мы также просим клиентов предоставить логотип и объекты фирменного стиля в формате .psd и .corel.</p>
                  	</div>
                  </div>

                  <div class="row justify-content-between mb-5">
                  	<div class="col-lg-3 text-lg-right">
                  		<h4 class="mb-4 text-primary">2. Поставка программного обеспечения</h4>
                  	</div>
                  	<div class="col-lg-8 text-black">
                  		<p>Вы становитесь обладателем программного обеспечения, а именно лицензионного ключа системы управления контентом «1С-Битрикс: Управление сайтом» редакция «Старт/Малый бизнес/Бизнес». В дальнейшем, именно на базе этого программного обеспечения будет происходить разработка интернет-магазина. При сдаче проекта мы передадим вам лицензионные ключи.</p>
                  	</div>
                  </div>

                  <div class="row justify-content-between mb-5">
                  	<div class="col-lg-3 text-lg-right">
                  		<h4 class="mb-4 text-primary">3. Разработка дизайн-макета интернет-магазина</h4>
                  	</div>
                  	<div class="col-lg-8 text-black">
                  		<p>После детального анализа информации о вашем бизнесе, мы приступаем к разработке макета интернет-магазина. На этом этапе мы приглашаем вас в офис на интервью с дизайнером и подробно рассказать о своих мыслях, идеях, пожеланиях. Если личная встреча невозможна, предлагаем пообщаться со специалистом по телефону или по скайпу.</p>
                  	</div>
                  </div>

                  <div class="row justify-content-between mb-5">
                  	<div class="col-lg-3 text-lg-right">
                  		<h4 class="mb-4 text-primary">4. Разработка и верстка</h4>
                  	</div>
                  	<div class="col-lg-8 text-black">
                  		<p>Макет утвержден, теперь можно приступить к непосредственной разработке интернет-магазина. Программисты и верстальщики пиксель в пиксель верстают макет, активируют лицензионные ключи, интегрируют сайт с CMS-системой, размещают его на хостинге, регистрируют и подключают доменное имя. Здесь же осуществляется настройка необходимого функционала (корзина, форма обратной связи, система заказов и т.д.).</p>
                  	</div>
                  </div>

                  <div class="row justify-content-between mb-5">
                  	<div class="col-lg-3 text-lg-right">
                  		<h4 class="mb-4 text-primary">5. Тестирование готового продукта</h4>
                  	</div>
                  	<div class="col-lg-8 text-black">
                  		<p>Когда разработка интернет-магазина окончена, его работоспособность проверяют наши тестировщики. В задачи этих специалистов входит проверка отображения сайта на различных устройствах и в браузерах, тестирование на наличие ошибок или каких-либо недочетов.</p>
                  	</div>
                  </div>

                  <div class="row justify-content-between mb-5">
                  	<div class="col-lg-3 text-lg-right">
                  		<h4 class="mb-4 text-primary">6. Сдача проекта и обучение</h4>
                  	</div>
                  	<div class="col-lg-8 text-black">
                  		<p>По завершении проекта мы приглашаем вас в офис, где в комфортной обстановке демонстрируем готовый продукт и обучаем управлению интернет-магазином. Здесь еще раз проверяется работоспособность сайта, а затем вам передается доступ к административной части.
						</p>
						<p>Также вам предоставляются подробные инструкции для самостоятельной работы с интернет-магазином. Мы рассказываем о регламентах обращения в техподдержку. Если у вас нет вопросов и нареканий, подписываем акт приема-передачи сайта. Для этого нужно взять с собой акт выполненных работ с подписью или печать организации.</p>
                  	</div>
                  </div>

                  <div class="row justify-content-between mb-5">
                  	<div class="col-lg-3 text-lg-right">
                  		<h4 class="mb-4 text-primary">7. Наполнение сайта контентом</h4>
                  	</div>
                  	<div class="col-lg-8 text-black">
                  		<p>Сама по себе разработка сайта, увы, не приносит коммерческих плодов. Для получения дохода с ресурса необходимо провести еще множество работ, а именно: наполнить интернет-магазин контентом, добавить товары и фото в каталог, подключить модули онлайн-оплаты. Все это можно сделать и самостоятельно, но если вы доверите работы профессионалам, это будет быстрее и качественнее.
Стоимость работ на данном этапе зависит от объема контента.</p>
                  	</div>
                  </div>

                  <div class="row justify-content-between mb-5">
                  	<div class="col-lg-3 text-lg-right">
                  		<h4 class="mb-4 text-primary">8. Реклама и продвижение </h4>
                  	</div>
                  	<div class="col-lg-8 text-black">
                  		<p>Интернет-магазин готов, он блестит и сияет, теперь нужно привлечь на него потенциальных покупателей. В ход идут контекстная реклама GoogleAdwords и Яндекс.Директ, реклама в социальных сетях, продвижение в поисковиках и многое другое. Доверьтесь на этом этапе нам, ведь мы точно знаем, что такое эффективный интернет-маркетинг.</p>
                  	</div>
                  </div>

                 
               </div>
            </div>
      
</section>

    <section class="slice pt-0 overflow-hidden-x mt-6">
         <div class="container text-center">
            <div class=row>
               <div class=col-md-12>
                  <span class="badge badge-primary badge-pill font-weight-normal">examples works</span>
                  <h5 class="h3 lh-180 mt-4 mb-6">Примеры работ</h5>
               </div>
            </div>
         </div>
         <div class=container-fluid>
            <div class="w-50 position-absolute bottom-0 right-n8 rotate-180"><img alt="Image placeholder" src="/img/svg/shapes/bubble-3.svg" class="svg-inject fill-secondary"></div>
            <div class=row>
               <div class="col-lg-3 col-sm-6">
                  <div class="card card-overlay card-hover-overlay hover-shadow-lg hover-translate-y-n10">
                     <figure class=figure><img alt="Image placeholder" src="/img/theme/light/img-v-1.jpg" class=img-fluid></figure>
                     <div class="card-img-overlay d-flex flex-column align-items-center p-0">
                        <div class="overlay-text w-75 mt-auto p-4">
                           <p class=lead>This is a wider card with supporting text below as a natural lead-in to additional content.</p>
                           <a href="business.html#!" class="link link-underline-white font-weight-bold">Marketing</a>
                        </div>
                        <div class="overlay-actions w-100 card-footer mt-auto d-flex justify-content-between align-items-center">
                           <div><a href="business.html#!" class="h6 mb-0">Living dangerously</a></div>
                           <div>
                              <div class=actions><a href="business.html#!" class="action-item mr-3"><i data-feather=paperclip></i></a> <a href="business.html#!" class="action-item mr-3"><i data-feather=eye></i></a> <a href="business.html#!" class=action-item><i data-feather=heart></i></a></div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-lg-3 col-sm-6">
                  <div class="card card-overlay card-hover-overlay hover-shadow-lg hover-translate-y-n10">
                     <figure class=figure><img alt="Image placeholder" src="/img/theme/light/img-v-2.jpg" class=img-fluid></figure>
                     <div class="card-img-overlay d-flex flex-column align-items-center p-0">
                        <div class="overlay-text w-75 mt-auto p-4">
                           <p class=lead>This is a wider card with supporting text below as a natural lead-in to additional content.</p>
                           <a href="business.html#!" class="link link-underline-white font-weight-bold">Marketing</a>
                        </div>
                        <div class="overlay-actions w-100 card-footer mt-auto d-flex justify-content-between align-items-center">
                           <div><a href="business.html#!" class="h6 mb-0">Living dangerously</a></div>
                           <div>
                              <div class=actions><a href="business.html#!" class="action-item mr-3"><i data-feather=paperclip></i></a> <a href="business.html#!" class="action-item mr-3"><i data-feather=eye></i></a> <a href="business.html#!" class=action-item><i data-feather=heart></i></a></div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-lg-3 col-sm-6">
                  <div class="card card-overlay card-hover-overlay hover-shadow-lg hover-translate-y-n10">
                     <figure class=figure><img alt="Image placeholder" src="/img/theme/light/img-v-3.jpg" class=img-fluid></figure>
                     <div class="card-img-overlay d-flex flex-column align-items-center p-0">
                        <div class="overlay-text w-75 mt-auto p-4">
                           <p class=lead>This is a wider card with supporting text below as a natural lead-in to additional content.</p>
                           <a href="business.html#!" class="link link-underline-white font-weight-bold">Marketing</a>
                        </div>
                        <div class="overlay-actions w-100 card-footer mt-auto d-flex justify-content-between align-items-center">
                           <div><a href="business.html#!" class="h6 mb-0">Living dangerously</a></div>
                           <div>
                              <div class=actions><a href="business.html#!" class="action-item mr-3"><i data-feather=paperclip></i></a> <a href="business.html#!" class="action-item mr-3"><i data-feather=eye></i></a> <a href="business.html#!" class=action-item><i data-feather=heart></i></a></div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-lg-3 col-sm-6">
                  <div class="card card-overlay card-hover-overlay hover-shadow-lg hover-translate-y-n10">
                     <figure class=figure><img alt="Image placeholder" src="/img/theme/light/img-v-4.jpg" class=img-fluid></figure>
                     <div class="card-img-overlay d-flex flex-column align-items-center p-0">
                        <div class="overlay-text w-75 mt-auto p-4">
                           <p class=lead>This is a wider card with supporting text below as a natural lead-in to additional content.</p>
                           <a href="business.html#!" class="link link-underline-white font-weight-bold">Marketing</a>
                        </div>
                        <div class="overlay-actions w-100 card-footer mt-auto d-flex justify-content-between align-items-center">
                           <div><a href="business.html#!" class="h6 mb-0">Living dangerously</a></div>
                           <div>
                              <div class=actions><a href="business.html#!" class="action-item mr-3"><i data-feather=paperclip></i></a> <a href="business.html#!" class="action-item mr-3"><i data-feather=eye></i></a> <a href="business.html#!" class=action-item><i data-feather=heart></i></a></div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>

<section class="slice slice-lg bg-section-dark">
         <div class=container>
            <div class="row mb-6 justify-content-center text-center">
               <div class="col-lg-8 col-md-10">
                  <h2 class="text-white mt-4">Вопрос-ответ</h2>
               </div>
            </div>
            <div class=row>
               <div class=col-xl-6>
                  <div id=accordion-1 class="accordion accordion-spaced">
                     <div class=card>
                        <div class="card-header py-4" id=heading-1-1 data-toggle=collapse role=button data-target=#collapse-1-1 aria-expanded=false aria-controls=collapse-1-1>
                           <h6 class="mb-0 text-black">Сколько времени уходит на разработку сайта?</h6>
                        </div>
                        <div id=collapse-1-1 class=collapse aria-labelledby=heading-1-1 data-parent=#accordion-1>
                           <div class=card-body>
                              <p class="text-black">Сроки зависят от сложности поставленных задач. Обычно от заполнения брифа до передачи готового сайта клиенту проходит месяц. Для получения точного расчета воспользуйтесь нашей бесплатной консультацией.</p>
                           </div>
                        </div>
                     </div>
                     <div class=card>
                        <div class="card-header py-4" id=heading-1-2 data-toggle=collapse role=button data-target=#collapse-1-2 aria-expanded=false aria-controls=collapse-1-2>
                           <h6 class="mb-0 text-black">Можно ли развить уже существующий сайт?</h6>
                        </div>
                        <div id=collapse-1-2 class=collapse aria-labelledby=heading-1-2 data-parent=#accordion-1>
                           <div class=card-body>
                              <p class="text-black">Такая возможность есть. Чтобы дать точный ответ, нам нужно ознакомиться с нюансами вашего проекта. Свяжитесь с нами, мы сделаем вам бесплатный аудит и предложим варианты развития.</p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class=col-xl-6>
                  <div id=accordion-2 class="accordion accordion-spaced">
                     <div class=card>
                        <div class="card-header py-4" id=heading-2-1 data-toggle=collapse role=button data-target=#collapse-2-1 aria-expanded=false aria-controls=collapse-2-1>
                           <h6 class="mb-0 text-black">Сколько стоит создание сайта?</h6>
                        </div>
                        <div id=collapse-2-1 class=collapse aria-labelledby=heading-2-1 data-parent=#accordion-2>
                           <div class=card-body>
                              <p class="text-black">Цена на услугу формируется с учетом таких факторов, как необходимый функционал, сложность структуры, объем контента, дополнительные услуги. Для получения точного расчета воспользуйтесь бесплатной консультацией.</p>
                           </div>
                        </div>
                     </div>
                     <div class=card>
                        <div class="card-header py-4" id=heading-2-2 data-toggle=collapse role=button data-target=#collapse-2-2 aria-expanded=false aria-controls=collapse-2-2>
                           <h6 class="mb-0 text-black">Почему сайты такие дорогие?</h6>
                        </div>
                        <div id=collapse-2-2 class=collapse aria-labelledby=heading-2-2 data-parent=#accordion-2>
                           <div class="card-body text-black">
                              <p>Неправильно делить сайты на дорогие и дешевые. Они либо окупаются, либо нет. Мы создаем продающие сайты для эффективного достижения целей клиента. Они помогают увеличить количество заявок, обеспечивают рост продаж, улучшают имидж компании. Нас часто спрашивают о возможности самостоятельного решения таких задач. В принципе, в интернете очень много онлайн-курсов и вебинаров на эту тему. Однако, для получения хорошего результата нужен практический опыт.</p>
<p>
В основе стоимости наших сайтов лежит успешный опыт реализации проектов. Конечно, у нас были и неудачи. Но даже из них наши клиенты могут извлечь пользу, так как такой опыт позволяет еще на старте оценить целесообразность дальнейших действий. Иногда мы советуем клиенту, не торопиться с созданием сайта, помогая ему сэкономить сотни тысяч рублей. Мы ценим свою репутацию и никогда не жертвуем ею ради быстрого дохода.</p>
<p>
На стоимость разработки сайта во многом влияет количество часов, затраченных на реализацию проекта. Средняя стоимость часа работы специалиста составляет 1000 рублей. Таких часов может быть как 50, так и 600. Качество сайта тоже будет разным. Многие компании жертвуют качеством для того, чтобы уложиться со сроками. Нужно понимать, что при таком подходе сотрудники работают в спешке и допускают ошибки, порой критичные. Поэтому мы сразу закладываем такое количество часов, которого будет достаточно для обдумывания идеи и ее качественной реализации.</p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>

<section class="slice slice-lg">
   <div class="container">
      <div class="jumbotron rounded-diagonal-right bg-dark border-0 rounded-lg py-5 mb-0">
         <div class="card-body px-5">
            <div class="row row-grid align-items-center">
               <div class="col-md-8">
                  <h4 class="text-white">Остались вопросы? </h4>
                  <p class="text-white">Поможем с выбором, ответим на все вопросы. <br>Подготовим для Вас индивидуальное предложение.</p>
               </div>
               <div class="col-12 col-md-4 text-md-right">
                  <a href="desktop-app.html#" class="btn btn-warning btn-icon">
                     <span class="btn-inner--text">Задать вопрос</span>
                     <span class="btn-inner--icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right">
                           <line x1="5" y1="12" x2="19" y2="12"></line>
                           <polyline points="12 5 19 12 12 19"></polyline>
                        </svg>
                     </span>
                  </a>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>






