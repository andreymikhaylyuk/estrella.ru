<?php

/* @var $this \yii\web\View */
/* @var $content string */


use yii\helpers\Html;
use app\assets\AppAsset;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\widgets\Setting;
use app\widgets\setcall\SetCall;

use yii\bootstrap\Modal;



AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <?= Html::csrfMetaTags() ?>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Estrella | Web and CRM systems development</title>
      <style>@keyframes hidePreloader{0%{width:100%;height:100%}100%{width:0;height:0}}body>div.preloader{position:fixed;background:#fff;width:100%;height:100%;z-index:1071;opacity:0;transition:opacity .5s ease;overflow:hidden;pointer-events:none;display:flex;align-items:center;justify-content:center}body:not(.loaded)>div.preloader{opacity:1}body:not(.loaded){overflow:hidden}body.loaded>div.preloader{animation:hidePreloader .5s linear .5s forwards}</style>
      <script>window.addEventListener("load",function(){setTimeout(function(){document.querySelector("body").classList.add("loaded")},300)})</script>
    <?php $this->head() ?>


</head>
<body>
      <div class=preloader>
         <div class="spinner-border text-primary" role=status><span class=sr-only>Загрузка...</span></div>
      </div>
      <header class=header-<?php
$earray = explode("/", $_SERVER['REQUEST_URI']);
$filename = $earray[count($earray)-1];
if ($filename == 'index.php' || $filename == '')
{?>
transparent
<? }?> id=header-main>
         <div id=navbar-top-main class="navbar-top py-1 navbar-white bg-dark navbar-border">
            <div class=container>
               <div class="navbar-nav align-items-center">
                  <div class="d-none d-lg-inline-block text-white text-sm"><span class="pr-4"><?= Setting::widget(['attr'=>'Номер телефона 1']) ?></span> <span>sale@estrella-studio.ru</span></div>
                  
                  <div class=ml-auto>
                     <ul class=nav>
                        <li class=nav-item><a class="nav-link text-white text-sm" href="#" target="_blank">Личный кабинет</a></li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>





         <nav class="navbar navbar-main navbar-expand-lg navbar-dark bg-black" id=navbar-main>
            <div class=container>
               <a class="navbar-brand logo_top" href="/"><img src="/img/<?php
$earray = explode("/", $_SERVER['REQUEST_URI']);
$filename = $earray[count($earray)-1];
if ($filename == 'index.php' || $filename == '')
{?>
logoestrellaagency1.svg
<? } else {?>
logoestrellaagency_black.svg
<?php } ?>" >







</a><button class=navbar-toggler type=button data-toggle=collapse data-target=#navbar-main-collapse aria-controls=navbar-main-collapse aria-expanded=false aria-label="Toggle navigation"><span class=navbar-toggler-icon></span></button>
               <div class="collapse navbar-collapse navbar-collapse-overlay" id=navbar-main-collapse>
                  <div class="collapse-header align-items-center">
                     <div class=col-6><a class=navbar-brand href="../../index.html"><img alt="Image placeholder" src="../../assets/img/brand/light-mono.svg"></a></div>
                     <div class="col-6 text-right"><button class=navbar-toggler type=button data-toggle=collapse data-target=#navbar-main-collapse aria-controls=navbar-main-collapse aria-expanded=false aria-label="Toggle navigation"><i data-feather=x></i></button></div>
                  </div>
                  <ul class="navbar-nav ml-auto">
                     <li class="nav-item dropdown dropdown-animate" data-toggle=hover>
                        <a class="nav-link dropdown-toggle font-weight-normal" data-toggle=dropdown href="#" aria-haspopup=true aria-expanded=false href="../../index.html">Компания</a>
                         <div class="dropdown-menu">
                         <div class="list-group list-group-flush">
                           <div class="dropdown dropdown-animate dropdown-submenu" >
                              <a href="/site/about" class="dropdown-item dropdown-toggle">О компании</a>
                           </div>
                           <div class="dropdown dropdown-animate dropdown-submenu" >
                              <a href="#" class="dropdown-item dropdown-toggle" >Вакансии</a>
                           </div>
                           <?php /* <div class="dropdown dropdown-animate dropdown-submenu" data-toggle=hover>
                              <a href="#" class="dropdown-item dropdown-toggle" >Отзывы</a>
                           </div> */ ?>
                           </div>
                        </div>
                     </li>
                     <li class="nav-item dropdown dropdown-animate" data-toggle=hover>
                        <a class="nav-link dropdown-toggle font-weight-normal" data-toggle=dropdown href="#" aria-haspopup=true aria-expanded=false>Услуги</a>
                        <div class="dropdown-menu">
                         <div class="list-group list-group-flush">
                           <div class="dropdown dropdown-animate dropdown-submenu">
                              <a href="/site/ecommerce" class="dropdown-item dropdown-toggle">eCommerce</a>
                           </div>
                           <div class="dropdown dropdown-animate dropdown-submenu">
                              <a href="/site/crm" class="dropdown-item dropdown-toggle">CRM системы</a>
                           </div>
                           <div class="dropdown dropdown-animate dropdown-submenu">
                              <a href="/site/corporate" class="dropdown-item dropdown-toggle">Корпоративные сайты</a>
                           </div>
                           <div class="dropdown dropdown-animate dropdown-submenu">
                              <a href="#" class="dropdown-item dropdown-toggle">Электронные сервисы</a>
                           </div>
                           <div class="dropdown dropdown-animate dropdown-submenu">
                              <a href="/site/mobile" class="dropdown-item dropdown-toggle">Мобильная разработка</a>
                           </div>
                            <div class="dropdown dropdown-animate dropdown-submenu">
                              <a href="#" class="dropdown-item dropdown-toggle">Проектирование UI/UX</a>
                           </div>
                           <div class="dropdown dropdown-animate dropdown-submenu">
                              <a href="#" class="dropdown-item dropdown-toggle">SEO Продвижение</a>
                           </div>
                           <div class="dropdown dropdown-animate dropdown-submenu" data-toggle=hover>
                              <a href="#" class="dropdown-item dropdown-toggle">Контекстная реклама</a>
                           </div>
                           <div class="dropdown dropdown-animate dropdown-submenu" data-toggle=hover>
                              <a href="#" class="dropdown-item dropdown-toggle">Техническая поддержка</a>
                           </div>
                           </div>
                        </div>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link font-weight-normal" href="/site/projects">Проекты</a>
                       <?php /* <div class=dropdown-menu>
                           <div class="dropdown dropdown-animate dropdown-submenu" data-toggle=hover>
                              <a href="#" class="dropdown-item dropdown-toggle" role=button data-toggle=dropdown aria-haspopup=true aria-expanded=false>eCommerce</a>
                           </div>
                           <div class="dropdown dropdown-animate dropdown-submenu" data-toggle=hover>
                              <a href="#" class="dropdown-item dropdown-toggle" role=button data-toggle=dropdown aria-haspopup=true aria-expanded=false>CRM системы</a>
                           </div>
                           <div class="dropdown dropdown-animate dropdown-submenu" data-toggle=hover>
                              <a href="#" class="dropdown-item dropdown-toggle" role=button data-toggle=dropdown aria-haspopup=true aria-expanded=false>Корпоративные сайты</a>
                           </div>
                           <div class="dropdown dropdown-animate dropdown-submenu" data-toggle=hover>
                              <a href="#" class="dropdown-item dropdown-toggle" role=button data-toggle=dropdown aria-haspopup=true aria-expanded=false>Мобильные приложения</a>
                           </div>
                           <div class="dropdown dropdown-animate dropdown-submenu" data-toggle=hover>
                              <a href="#" class="dropdown-item dropdown-toggle" role=button data-toggle=dropdown aria-haspopup=true aria-expanded=false>Дизайн</a>
                           </div>
                        </div> */ ?>
                     </li>
                     <li class=nav-item><a class="nav-link font-weight-normal" href="../../index.html">Новости</a></li>
                     <li class=nav-item><a class="nav-link font-weight-normal" href="/site/contact">Контакты</a></li>
                  </ul>
                  <a class="btn btn-sm d-none d-lg-inline-block btn-white font-weight-normal ml-auto text-black" data-toggle="modal" data-target="#exampleModalCenter">Заказать звонок</a>
                  <div class="d-lg-none p-4 text-center"><a href="#" class="btn btn-block btn-sm btn-neutral rounded-pill" data-toggle="modal" data-target="#exampleModalCenter">Заказать звонок</a></div>
               </div>
            </div>
         </nav>
      </header>
<?php $this->beginBody() ?>


<?php if (Yii::$app->session->has('contactFormSubmitted')) { ?>

    <?php 
    $this->registerJs(
        "$('#myModalSendOk').modal('show');",
        yii\web\View::POS_READY
    );
    ?>

    <!-- Modal -->
    <div class="modal fade" id="myModalSendOk" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">

                <div class="modal-body">
                    <div style="text-align: center">
                        <h4>Спасибо! Мы с Вами свяжемся!</h4>
                        <br>
                        <button type="button" class="button" data-dismiss="modal">Закрыть</button>
                    </div>

                </div>

            </div>
        </div>
    </div>

<?php } ?>
<header>



<?php /*
    <div class="main-container">
        <nav class="navbar navbar-expand-lg">
            <a class="navbar-brand logo" href="<?= Url::home()?>"><img src="/img/logo_stone_garden_v2.png" style="max-width: 100px;filter: drop-shadow(0px 0px 5px #000) "></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="<?=Url::toRoute(['site/services'])?>">Услуги</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?=Url::toRoute(['site/projects'])?>">Галерея</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?=Url::toRoute(['site/advantages'])?>">Преимущества</a>
                    </li>
                    <!--<li class="nav-item">-->
                    <!--    <a class="nav-link" href="<?//=Url::toRoute(['site/stages'])?>">Этапы работы</a>-->
                    <!--</li>-->
                    <li class="nav-item">
                        <a class="nav-link" href="<?=Url::toRoute(['site/contacts'])?>">Контакты</a>
                    </li>
                </ul>
                <div class="my-2 my-lg-0 nav-phone">
                    <a><?= Setting::widget(['attr'=>'Номер телефона 1']) ?></a>
                </div>
            </div>
        </nav>
        <div class="row">
            <div class="col-md-12 col-lg-8">
                <h1>Ландшафтный дизайн
                    и озеленение</h1>
                <p>В Воронеже <br>
                    Белгороде и по всей России</p>
            </div>
            <div class="col-md-12 col-lg-4">
                <div class="form-header">
                    <h3>Закажите бесплатную
                        консультацию специалиста
                        с выездом на Ваш участок</h3>
                    <?= SetCall::widget() ?>



                </div>
            </div>
        </div>
    </div>


</header> */ ?>


<?= $content ?>

<footer id=footer-main>
         <div class="footer footer-light">
            <div class=container>
               <div class="row pt-md">
                  <div class="col-lg-4 mb-5 mb-lg-0">
                     <h3 class="logo_top" style=""><img src="/img/logoestrellaagency_black.svg" style="height: 30px"></h3>
                     <p class="text-black mb-0 mb-2">8 4722 36 50 21</p>
                     <p class="text-black text-sm mb-0">sale@estrella.agency</p>
                     <p class="text-black text-sm">г.Белгород, Б-Р Народный 70, офис 417</p>
                  </div>
                  <div class="col-lg-2 col-6 col-sm-4 offset-lg-1 mb-5 mb-lg-0">
                     <h6 class="heading mb-3 text-black">Компания</h6>
                     <ul class=list-unstyled>
                        <li><a href="/site/about" class="text-sm text-black">О компании</a></li>
                        <li><a href="#" class="text-sm text-black">Вакансии</a></li>
                        <li><a href="#" class="text-sm text-black">Отзывы</a></li>
                        <li><a href="#" class="text-sm text-black">Новости</a></li>
                        <li><a href="/site/contact" class="text-sm text-black">Контакты</a></li>
                     </ul>
                  </div>
                  <div class="col-lg-2 col-6 col-sm-4 mb-5 mb-lg-0">
                     <h6 class="heading mb-3 text-black">Разработка</h6>
                     <ul class="list-unstyled text-small">
                        <li><a href="#" class="text-sm text-black">eCommerce</a></li>
                        <li><a href="#" class="text-sm text-black">CRM системы</a></li>
                        <li><a href="#" class="text-sm text-black">Сайт компании</a></li>
                        <li><a href="#" class="text-sm text-black">Лендинг</a></li>
                     </ul>
                  </div>
                  <div class="col-lg-3 col-sm-4 mb-5 mb-lg-0">
                     <h6 class="heading mb-3 text-black">Продвижение</h6>
                     <ul class=list-unstyled>
                        <li><a href="#" class="text-sm text-black">SEO продвижение</a></li>
                        <li><a href="#" class="text-sm text-black">Контекстная реклама</a></li>
                        <li><a href="#" class="text-sm text-black">SMM продвижение</a></li>
                     </ul>
                  </div>
               </div>
               <div class="row align-items-center justify-content-md-between py-4 mt-4 border-top mx-0">
                  <div class="col-md-6 mb-3 mb-md-0">
                     <div class="copyright text-sm text-black text-center text-md-left">&copy; 2019 <a href="https://estrella-studio.ru" class="text-black" style="border-bottom: 1px solid;" target=_blank>estrella</a> - Web and CRM systems development</div>
                  </div>
                  <div class=col-md-6>
                     <ul class="nav align-items-center justify-content-center justify-content-md-end">
                        <li class="nav-item dropdown border-right pr-2">
                           <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right"><a href="#" class=dropdown-item><img alt="Image placeholder" src="../../assets/img/icons/flags/es.svg" class="svg-inject icon-flag">Spanish</a> <a href="#" class=dropdown-item><img alt="Image placeholder" src="../../assets/img/icons/flags/us.svg" class="svg-inject icon-flag">English</a> <a href="#" class=dropdown-item><img alt="Image placeholder" src="../../assets/img/icons/flags/gr.svg" class="svg-inject icon-flag">Greek</a></div>
                        </li>
                        <li class=nav-item><a class=nav-link href="/site/policy">Политика конфедициальности</a></li>
                        <li class=nav-item><a class="nav-link pr-0" href="#">Использование Cookie</a></li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </footer>
      <div class="modal fade" tabindex=-1 role=dialog id=modal-cookies data-backdrop=false aria-labelledby=modal-cookies aria-hidden=true>
         <div class="modal-dialog modal-dialog-aside left-4 right-4 bottom-4">
            <div class="modal-content bg-dark">
               <div class=modal-body>
                  <p class="text-sm text-white mb-3">Мы используем файлы cookie. Используя наш веб-сайт, вы соглашаетесь на использование нами файлов cookie.</p>
                  <a href="/site/cookie" class="btn btn-sm btn-neutral" target=_blank>Подробнее</a> <button type=button class="btn btn-sm btn-primary mr-2" data-dismiss=modal>ОК</button>
               </div>
            </div>
         </div>
      </div>

      <!-- Button trigger modal -->




<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Закажите звонок или оставьте заявку</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div style="text-align: center">
                    <?php
                    $form = ActiveForm::begin(); ?>

                    <div class="form-group">
                      <input class="form-control form-control-lg" type="text" type="text" name="surname" placeholder="Имя" required="">
                    </div>
                    <div class="form-group">
                      <input class="form-control form-control-lg" type="text" name="phone" placeholder="Телефон" required="">
                    </div>
                    <!-- <input type="text" name="surname" placeholder="Имя"> -->

                    <!-- <input type="text" name="phone" placeholder="Телефон"> -->

                  <!-- <div class="text-center">
                    <button type="submit" class="btn btn-block btn-lg btn-primary mt-4">Send your message</button>
                  </div> -->

                  <div class="text-center">
                    <?= Html::submitButton('Отправить', ['class' => 'btn btn-block btn-lg btn-primary mt-4']) ?>
                  </div>
                  <div class="text-center mt-4">
                    <p class="text-small lh-120 mb-0"><small>Нажимая кнопку Отправить, Вы соглашаетесь с <br><a href="/site/policy" target="_blank">Политикой обработки персональных данных</a></small></p>
                  </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>


<?php $this->endBody() ?>


<script type="text/javascript">
   $(document).ready(function(){
      $(".owl-carousel").owlCarousel({
      // center: true,
    items:3,
    loop:true,
    margin:40,
    // nav: true,
    dots: true,
    autoplay: true,
    responsive:{
        600:{
            items:7
        }
    }
      });

      $(".owl-carousel-2").owlCarousel({
      // center: true,
   items:1,
   nav: false,
   dots: false,
    margin:10,
    autoHeight:true
      });
});
</script>


</body>
</html>
<?php $this->endPage() ?>
