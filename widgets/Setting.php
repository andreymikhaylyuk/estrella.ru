<?php

namespace app\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use app\models\Settings;
class Setting extends Widget
{
    public $message;
    public $attr;

    public function init(){
        parent::init();
        if ($this->attr === null){
            $this->attr = '+79999999999';
        }
    }

    public function run(){
        $model = Settings::findOne(['name' => $this->attr]);
        return Html::encode($model->content);
    }
}