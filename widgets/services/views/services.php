<?php


use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

  

            <?php foreach ($models as $model):
                $image = $model->getImage();
                //die;
                ?>

                 <div class="masonry-item col-xl-4 col-md-6">
                  <div class="card hover-translate-y-n3 hover-shadow-lg overflow-hidden">
                     <div class="position-relative overflow-hidden"><a href="blog-grid.html#" class=d-block>
                        <?= Html::img(Yii::getAlias('@web/upload/photos/').$image->filePath, ['class' => 'card-img-top', 'style' => 'height: 300px;oveflow: hidden;object-fit: cover;']) ?></a></div>
                     <div class="card-body py-4">
                        <a href="blog-grid.html#" class="h5 stretched-link lh-110"><?= Html::encode($model->title) ?></a>
                        <p class="mt-3 mb-0 lh-130" style="height: 60px;overflow: hidden;"><small><?= Html::encode($model->content) ?></small></p>
                     </div>
                    
                  </div>
               </div>



            <?php endforeach; ?>
      
