<?php
namespace app\widgets\services;

use app\models\Pages;

class ServicesWidget extends \yii\base\Widget
{
    public function init()
    {
        return parent::init();
    }

    public function run()
    {
        /*
         * Какое-либо действие, логика, создание модели и тд
         */
        $models = Pages::find()->where(['category' => 'Услуги'])->andWhere(['status' => '1'])->limit(3)->all();

        return $this->render('services', [
            'models' => $models,
        ]);
    }
}