<?php
namespace app\widgets\setcall;

class SetCall extends \yii\base\Widget
{
    public function init()
    {
        return parent::init();
    }

    public function run()
    {
        /*
         * Какое-либо действие, логика, создание модели и тд
         */

        return $this->render('form');
    }
}