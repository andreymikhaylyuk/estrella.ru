<?php
namespace app\widgets\galery;

use app\models\Pages;

class GaleryWidget extends \yii\base\Widget
{
    public function init()
    {
        return parent::init();
    }

    public function run()
    {
        /*
         * Какое-либо действие, логика, создание модели и тд
         */
        $models = Pages::find()->where(['category' => 'Проекты'])->andWhere(['status' => '1'])->all();

        return $this->render('galery', [
            'models' => $models,
        ]);
    }
}