<?php


use yii\helpers\Html;

?>
    <div class="main-container">
        <h2> Галерея </h2>
        <div class="row">

            <?php foreach ($models as $model):
                $image = $model->getImage();
            ?>


                <div class="col-lg-4 col-md-6 item">
                    <!--<h3><?//= Html::encode($model->title) ?> </h3>-->
                    <div class="services-images">
                        <?= Html::img(Yii::getAlias('@web/upload/photos/').$image->filePath) ?>
                    </div>
                    <div style="text-align: center">
                    <?= Html::button("<span class='glyphicon glyphicon-plus' aria-hidden='true'>Подробнее</span>",
                    ['class'=>'button',
                        'onclick'=> "window.location.href = '" . \Yii::$app->urlManager->createUrl(['/site/projects-about','id'=>$model->id]) . "';",
                    ]
                ) ?> 
                    </div>

                </div>

            <?php endforeach; ?>
        </div>


    </div>
