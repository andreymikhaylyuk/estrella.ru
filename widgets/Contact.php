<?php

namespace app\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use app\models\Settings;
class Setting extends Widget
{
    public $message;
    public $attr;

    public function init(){
        parent::init();
        if ($this->message === null){
            $this->message = '+79999999999';
        }
    }

    public function run(){
        $model = Settings::find()->where(['name' => $this->attr])->one();
        $this->message = $model->content;
        //return var_dump($model->content);
        return Html::encode($this->message);
    }
}