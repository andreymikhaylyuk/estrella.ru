<?php

return [
    /*'/logout/'                 => 'authorization/logout',
    '/login/'                  => 'authorization/login',
    '/confirm-email/<token>/'  => 'authorization/confirm-email',
    '/request-password-reset/' => 'authorization/request-password-reset',
    '/password-reset/<token>/' => 'authorization/password-reset',

    '/for-organizations/'         => 'information/for-organizations',
    '/organization-add/'          => 'information/organization-add',
    '/confirm-bid-email/<token>/' => 'information/confirm-bid-email',

    '/registration/' => 'user/registration',

    '/support-request/' => 'support/create-support-request',

    '/catalog/<category>/'  => 'organizations/catalog',
    '/catalog/'             => 'organizations/catalog',
    '/organization-search/' => 'organizations/search',
    '/organization/<slug>'  => 'organizations/organization',
    '/landing/<slug>'       => 'organizations/landing',
    '/discounts/<slug>'     => 'organizations/discounts',*/

    '<id:([0-9])+>/images/image-by-item-and-alias' => 'yii2images/images/image-by-item-and-alias',

    [
        'pattern' => '<controller>/<action>/<id:\d+>',
        'route'   => '<controller>/<action>',
    ],
    [
        'pattern' => '<controller>/<action>',
        'route'   => '<controller>/<action>',
    ],
    [
        'pattern' => '<module>/<controller>/<action>/<id:\d+>',
        'route'   => '<module>/<controller>/<action>',
    ],
    [
        'pattern' => '<module>/<controller>/<action>',
        'route'   => '<module>/<controller>/<action>',
    ],
];
